﻿using Microsoft.EntityFrameworkCore;
using TheTrader.Domain.Models;

namespace TheTrader.EntityFramework
{
    public class TheTraderDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<AssetTransaction> AssetTransactions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AssetTransaction>().OwnsOne(at => at.Stock);

            base.OnModelCreating(modelBuilder);
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=TheTrader;Integrated Security=True;"
            );
            base.OnConfiguring(optionsBuilder);
        }
    }
}
