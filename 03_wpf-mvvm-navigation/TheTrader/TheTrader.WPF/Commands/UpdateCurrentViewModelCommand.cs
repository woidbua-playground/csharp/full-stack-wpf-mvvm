﻿using System;
using System.Windows.Input;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class UpdateCurrentViewModelCommand : ICommand
    {
        private readonly INavigator _navigator;


        public UpdateCurrentViewModelCommand(INavigator navigator)
        {
            _navigator = navigator;
        }


        public event EventHandler? CanExecuteChanged;


        public bool CanExecute(object parameter)
        {
            return true;
        }


        public void Execute(object parameter)
        {
            if (!(parameter is ViewType viewType))
                return;

            _navigator.CurrentViewModel = viewType switch
            {
                ViewType.Home => new HomeViewModel(),
                ViewType.Portfolio => new PortfolioViewModel(),
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }
}
