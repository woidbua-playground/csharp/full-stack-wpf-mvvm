﻿using System.Windows.Input;
using TheTrader.WPF.Commands;
using TheTrader.WPF.Models;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.State.Navigators
{
    public class Navigator : ObservableObject, INavigator
    {
        private ViewModelBase? _currentViewModel;
        private ICommand? _updateCurrentViewModelCommand;

        public ViewModelBase? CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                if (_currentViewModel == value)
                    return;

                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

        public ICommand UpdateCurrentViewModelCommand =>
            _updateCurrentViewModelCommand ??= new UpdateCurrentViewModelCommand(this);
    }
}
