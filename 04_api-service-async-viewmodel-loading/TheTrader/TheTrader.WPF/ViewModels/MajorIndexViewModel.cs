﻿using TheTrader.Domain.Models;
using TheTrader.Domain.Services;

namespace TheTrader.WPF.ViewModels
{
    public class MajorIndexViewModel
    {
        private readonly IMajorIndexService _majorIndexService;


        public MajorIndexViewModel(IMajorIndexService majorIndexService)
        {
            _majorIndexService = majorIndexService;
        }


        public MajorIndex? DowJones { get; private set; }

        public MajorIndex? Nasdaq { get; private set; }

        public MajorIndex? Sp500 { get; private set; }


        public static MajorIndexViewModel LoadMajorIndexViewModel(IMajorIndexService majorIndexService)
        {
            var majorIndexViewModel = new MajorIndexViewModel(majorIndexService);
            majorIndexViewModel.LoadMajorIndexes();
            return majorIndexViewModel;
        }


        private void LoadMajorIndexes()
        {
            _majorIndexService.GetMajorIndex(MajorIndexType.DowJones)
                              .ContinueWith(
                                  task =>
                                  {
                                      if (task.Exception == null)
                                          DowJones = task.Result;
                                  }
                              );
            _majorIndexService.GetMajorIndex(MajorIndexType.Nasdaq)
                              .ContinueWith(
                                  task =>
                                  {
                                      if (task.Exception == null)
                                          Nasdaq = task.Result;
                                  }
                              );
            _majorIndexService.GetMajorIndex(MajorIndexType.Sp500)
                              .ContinueWith(
                                  task =>
                                  {
                                      if (task.Exception == null)
                                          Sp500 = task.Result;
                                  }
                              );
        }
    }
}
