﻿using TheTrader.Domain.Services;

namespace TheTrader.Domain.Models
{
    public class MajorIndex
    {
        public double Price { get; set; }

        public double Changes { get; set; }

        public MajorIndexType Type { get; set; }
    }
}