﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services;

namespace TheTrader.FinancialModelingPrepApi.Services
{
    public class MajorIndexService : IMajorIndexService
    {
        private const string BaseUri = "https://financialmodelingprep.com/api/v3/majors-indexes";
        private const string ApiKey = "cdefd43f5c716bb3228823d9f32e3a58";


        public async Task<MajorIndex> GetMajorIndex(MajorIndexType indexType)
        {
            var uri = $"{BaseUri}/{GetUriSuffix(indexType)}?apikey={ApiKey}";

            using var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(uri);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var majorIndex = JsonConvert.DeserializeObject<MajorIndex>(jsonResponse);
            majorIndex.Type = indexType;
            return majorIndex;
        }


        private static string GetUriSuffix(MajorIndexType indexType)
        {
            return indexType switch
            {
                MajorIndexType.Nasdaq => ".IXIC",
                MajorIndexType.Sp500 => ".INX",
                _ => ".DJI"
            };
        }
    }
}
