﻿using System.Windows.Input;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.State.Navigators
{
    public interface INavigator
    {
        ViewModelBase? CurrentViewModel { get; set; }

        ICommand UpdateCurrentViewModelCommand { get; }
    }
}
