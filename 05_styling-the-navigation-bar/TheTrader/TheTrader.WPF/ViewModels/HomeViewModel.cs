﻿namespace TheTrader.WPF.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        public HomeViewModel(MajorIndexViewModel majorIndexViewModel)
        {
            MajorIndexViewModel = majorIndexViewModel;
        }


        public MajorIndexViewModel MajorIndexViewModel { get; }
    }
}
