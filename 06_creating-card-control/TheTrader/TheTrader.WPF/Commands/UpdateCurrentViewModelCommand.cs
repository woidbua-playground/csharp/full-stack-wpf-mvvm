﻿using System;
using System.Windows.Input;
using TheTrader.FinancialModelingPrepApi.Services;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class UpdateCurrentViewModelCommand : ICommand
    {
        private readonly INavigator _navigator;

        private HomeViewModel? _homeViewModel;
        private PortfolioViewModel? _portfolioViewModel;


        public UpdateCurrentViewModelCommand(INavigator navigator)
        {
            _navigator = navigator;
        }


        public event EventHandler? CanExecuteChanged;


        public bool CanExecute(object parameter)
        {
            return true;
        }


        public void Execute(object parameter)
        {
            if (!(parameter is ViewType viewType))
                return;

            _navigator.CurrentViewModel = viewType switch
            {
                ViewType.Home => _homeViewModel ??= new HomeViewModel(CreateMajorIndexViewModel()),
                ViewType.Portfolio => _portfolioViewModel ??= new PortfolioViewModel(),
                _ => throw new ArgumentOutOfRangeException()
            };
        }


        private static MajorIndexListingViewModel CreateMajorIndexViewModel()
        {
            return MajorIndexListingViewModel.LoadMajorIndexViewModel(new MajorIndexService());
        }


        protected void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
