﻿using System;
using TheTrader.FinancialModelingPrepApi.Services;

namespace TheTrader.ConsoleApp
{
    public static class Program
    {
        private static void Main()
        {
            var stockPriceService = new StockPriceService();

            var price = stockPriceService.GetPrice("AAPL").GetAwaiter().GetResult();
            Console.WriteLine(price);
        }
    }
}
