﻿using System;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Exceptions
{
    public class InvalidMajorIndexException : Exception
    {
        public InvalidMajorIndexException(MajorIndexType majorIndexType)
        {
            MajorIndexType = majorIndexType;
        }


        public InvalidMajorIndexException(MajorIndexType majorIndexType, string message)
            : base(message)
        {
            MajorIndexType = majorIndexType;
        }


        public InvalidMajorIndexException(MajorIndexType majorIndexType, string message, Exception innerException)
            : base(message, innerException)
        {
            MajorIndexType = majorIndexType;
        }


        public MajorIndexType MajorIndexType { get; set; }
    }
}
