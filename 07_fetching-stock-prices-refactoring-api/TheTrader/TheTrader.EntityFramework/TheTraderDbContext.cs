﻿using Microsoft.EntityFrameworkCore;
using TheTrader.Domain.Models;

namespace TheTrader.EntityFramework
{
    public class TheTraderDbContext : DbContext
    {
        public TheTraderDbContext(DbContextOptions options)
            : base(options)
        {
        }


        public DbSet<User> Users { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<AssetTransaction> AssetTransactions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AssetTransaction>().OwnsOne(at => at.Asset);

            base.OnModelCreating(modelBuilder);
        }
    }
}
