﻿namespace TheTrader.FinancialModelingPrepApi
{
    public class FinancialModelingPrepHttpClientFactory
    {
        private readonly string _apiKey;


        public FinancialModelingPrepHttpClientFactory(string apiKey)
        {
            _apiKey = apiKey;
        }


        public FinancialModelingPrepHttpClient Create()
        {
            return new FinancialModelingPrepHttpClient(_apiKey);
        }
    }
}
