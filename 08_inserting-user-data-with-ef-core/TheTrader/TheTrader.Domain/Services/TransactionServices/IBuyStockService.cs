﻿using System.Threading.Tasks;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services.TransactionServices
{
    public interface IBuyStockService
    {
        Task<Account> BuyStock(Account buyer, string symbol, int shares);
    }
}
