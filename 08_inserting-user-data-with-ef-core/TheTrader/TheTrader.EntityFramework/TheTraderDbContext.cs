﻿using Microsoft.EntityFrameworkCore;
using TheTrader.Domain.Models;
using TheTrader.WPF.Annotations;

#pragma warning disable 8618
namespace TheTrader.EntityFramework
{
    public class TheTraderDbContext : DbContext
    {
        public TheTraderDbContext(DbContextOptions options)
            : base(options)
        {
        }


        [UsedImplicitly]
        public DbSet<User> Users { get; set; }

        [UsedImplicitly]
        public DbSet<Account> Accounts { get; set; }

        [UsedImplicitly]
        public DbSet<AssetTransaction> AssetTransactions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AssetTransaction>().OwnsOne(at => at.Asset);

            base.OnModelCreating(modelBuilder);
        }
    }
}
