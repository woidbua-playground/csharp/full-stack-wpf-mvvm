﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheTrader.FinancialModelingPrepApi
{
    public class FinancialModelingPrepHttpClient : HttpClient
    {
        private const string BaseUri = "https://financialmodelingprep.com/api/v3/";

        private readonly string _apiKey;


        public FinancialModelingPrepHttpClient(string apiKey)
        {
            _apiKey = apiKey;
            BaseAddress = new Uri(BaseUri);
        }


        public async Task<T> GetAsync<T>(string uri)
        {
            var response = await GetAsync($"{uri}?apikey={_apiKey}");
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<T>(jsonResponse);
            return result;
        }
    }
}
