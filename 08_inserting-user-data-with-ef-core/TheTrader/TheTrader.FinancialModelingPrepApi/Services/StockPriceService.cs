﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Services;
using TheTrader.FinancialModelingPrepApi.Results;

namespace TheTrader.FinancialModelingPrepApi.Services
{
    public class StockPriceService : IStockPriceService
    {
        private const string BaseUri = "quote-short";

        private readonly FinancialModelingPrepHttpClientFactory _httpClientFactory;


        public StockPriceService(FinancialModelingPrepHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }


        public async Task<double> GetPrice(string symbol)
        {
            var uri = $"{BaseUri}/{symbol}";

            using var httpClient = _httpClientFactory.Create();
            var stockPriceResults = await httpClient.GetAsync<IList<StockPriceResult>>(uri);

            if (!stockPriceResults.Any())
                throw new InvalidSymbolException(symbol);

            return stockPriceResults.First().Price;
        }
    }
}
