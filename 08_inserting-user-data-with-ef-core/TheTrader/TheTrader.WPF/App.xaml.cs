﻿using System.Windows;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var window = new MainWindow { DataContext = new MainViewModel() };
            window.Show();

            base.OnStartup(e);
        }


        //private IServiceProvider CreateServiceProvider()
        //{
        //    IServiceCollection services = new ServiceCollection();

        //    string apiKey = ConfigurationManager.AppSettings.Get("financeApiKey");
        //    services.AddSingleton<FinancialModelingPrepHttpClientFactory>(
        //        new FinancialModelingPrepHttpClientFactory(apiKey)
        //    );
        //}
    }
}
