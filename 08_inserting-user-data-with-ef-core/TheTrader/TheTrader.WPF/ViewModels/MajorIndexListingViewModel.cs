﻿using TheTrader.Domain.Models;
using TheTrader.Domain.Services;

namespace TheTrader.WPF.ViewModels
{
    public class MajorIndexListingViewModel : ViewModelBase
    {
        private readonly IMajorIndexService _majorIndexService;

        private MajorIndex? _dowJones;
        private MajorIndex? _nasdaq;
        private MajorIndex? _sp500;


        public MajorIndexListingViewModel(IMajorIndexService majorIndexService)
        {
            _majorIndexService = majorIndexService;
        }


        public MajorIndex? DowJones
        {
            get => _dowJones;
            private set
            {
                if (_dowJones == value)
                    return;

                _dowJones = value;
                OnPropertyChanged();
            }
        }

        public MajorIndex? Nasdaq
        {
            get => _nasdaq;
            private set
            {
                if (_nasdaq == value)
                    return;

                _nasdaq = value;
                OnPropertyChanged();
            }
        }

        public MajorIndex? Sp500
        {
            get => _sp500;
            private set
            {
                if (_sp500 == value)
                    return;

                _sp500 = value;
                OnPropertyChanged();
            }
        }


        public static MajorIndexListingViewModel LoadMajorIndexViewModel(IMajorIndexService majorIndexService)
        {
            var majorIndexViewModel = new MajorIndexListingViewModel(majorIndexService);
            majorIndexViewModel.LoadMajorIndexes();
            return majorIndexViewModel;
        }


        private void LoadMajorIndexes()
        {
            _majorIndexService.GetMajorIndex(MajorIndexType.DowJones)
                              .ContinueWith(
                                  task =>
                                  {
                                      if (task.Exception == null)
                                          DowJones = task.Result;
                                  }
                              );
            _majorIndexService.GetMajorIndex(MajorIndexType.Nasdaq)
                              .ContinueWith(
                                  task =>
                                  {
                                      if (task.Exception == null)
                                          Nasdaq = task.Result;
                                  }
                              );
            _majorIndexService.GetMajorIndex(MajorIndexType.Sp500)
                              .ContinueWith(
                                  task =>
                                  {
                                      if (task.Exception == null)
                                          Sp500 = task.Result;
                                  }
                              );
        }
    }
}
