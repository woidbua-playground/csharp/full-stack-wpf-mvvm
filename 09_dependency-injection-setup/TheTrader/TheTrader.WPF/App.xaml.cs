﻿using System;
using System.Configuration;
using System.Windows;
using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Extensions;
using TheTrader.EntityFramework.Extensions;
using TheTrader.FinancialModelingPrepApi.Extensions;
using TheTrader.WPF.Extensions;

namespace TheTrader.WPF
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var serviceProvider = CreateServiceProvider();

            var window = serviceProvider.GetRequiredService<MainWindow>();
            window.Show();

            base.OnStartup(e);
        }


        private static IServiceProvider CreateServiceProvider()
        {
            IServiceCollection services = new ServiceCollection();

            services.AddDomainServices();
            services.AddEntityFrameworkServices();
            services.AddFinancialModelingPrepApiServices(GetFinancialModelingPrepApiKey());
            services.AddWpfServices();

            return services.BuildServiceProvider();
        }


        private static string GetFinancialModelingPrepApiKey()
        {
            return ConfigurationManager.AppSettings.Get("financeApiKey");
        }
    }
}
