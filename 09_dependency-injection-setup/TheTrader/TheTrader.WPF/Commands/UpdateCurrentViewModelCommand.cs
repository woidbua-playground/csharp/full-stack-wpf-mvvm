﻿using System;
using System.Windows.Input;
using TheTrader.WPF.Factories;
using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.Commands
{
    public class UpdateCurrentViewModelCommand : ICommand
    {
        private readonly INavigator _navigator;
        private readonly IViewModelAbstractFactory _viewModelAbstractFactory;


        public UpdateCurrentViewModelCommand(INavigator navigator, IViewModelAbstractFactory viewModelAbstractFactory)
        {
            _navigator = navigator;
            _viewModelAbstractFactory = viewModelAbstractFactory;
        }


        public event EventHandler? CanExecuteChanged;


        public bool CanExecute(object parameter)
        {
            return true;
        }


        public void Execute(object parameter)
        {
            if (!(parameter is ViewType viewType))
                return;

            _navigator.LoadedViewModels.TryGetValue(viewType, out var existingViewModel);
            if (existingViewModel != null)
            {
                _navigator.CurrentViewModel = existingViewModel;
            }
            else
            {
                var newViewModel = _viewModelAbstractFactory.CreateViewModel(viewType);
                _navigator.LoadedViewModels.Add(viewType, newViewModel);
                _navigator.CurrentViewModel = newViewModel;
            }
        }


        protected void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
