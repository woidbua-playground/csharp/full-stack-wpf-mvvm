﻿using System.Windows;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF
{
    public partial class MainWindow : Window
    {
        public MainWindow(ViewModelBase dataContext)
        {
            InitializeComponent();
            DataContext = dataContext;
        }
    }
}
