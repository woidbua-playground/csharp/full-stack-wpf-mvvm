﻿using System.Collections.Generic;
using System.Windows.Input;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.State.Navigators
{
    public interface INavigator
    {
        IDictionary<ViewType, ViewModelBase> LoadedViewModels { get; }

        ViewModelBase? CurrentViewModel { get; set; }

        ICommand UpdateCurrentViewModelCommand { get; }
    }
}
