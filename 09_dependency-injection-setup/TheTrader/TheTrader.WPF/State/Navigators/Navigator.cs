﻿using System.Collections.Generic;
using System.Windows.Input;
using TheTrader.WPF.Commands;
using TheTrader.WPF.Factories;
using TheTrader.WPF.Models;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.State.Navigators
{
    public class Navigator : ObservableObject, INavigator
    {
        private ViewModelBase? _currentViewModel;


        public Navigator(IViewModelAbstractFactory viewModelAbstractFactory)
        {
            UpdateCurrentViewModelCommand = new UpdateCurrentViewModelCommand(this, viewModelAbstractFactory);
        }


        public IDictionary<ViewType, ViewModelBase> LoadedViewModels { get; } =
            new Dictionary<ViewType, ViewModelBase>();

        public ViewModelBase? CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                if (_currentViewModel == value)
                    return;

                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

        public ICommand UpdateCurrentViewModelCommand { get; }
    }
}
