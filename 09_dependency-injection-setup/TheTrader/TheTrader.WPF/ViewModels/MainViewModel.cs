﻿using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel(INavigator navigator)
        {
            Navigator = navigator;
            Navigator.UpdateCurrentViewModelCommand.Execute(ViewType.Home);
        }


        public INavigator Navigator { get; }
    }
}
