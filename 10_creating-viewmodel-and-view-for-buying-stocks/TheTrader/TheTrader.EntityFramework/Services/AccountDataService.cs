﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using TheTrader.Domain.Models;

namespace TheTrader.EntityFramework.Services
{
    public class AccountDataService : GenericDataService<Account>
    {
        public AccountDataService(TheTraderDbContextFactory contextFactory)
            : base(contextFactory)
        {
        }


        protected override IQueryable<Account> GetDbSetQueryable(TheTraderDbContext context)
        {
            return context.Accounts.Include(a => a.AssetTransactions);
        }
    }
}
