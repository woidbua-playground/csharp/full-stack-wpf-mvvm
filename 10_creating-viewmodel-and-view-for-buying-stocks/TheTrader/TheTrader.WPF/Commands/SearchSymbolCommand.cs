﻿using System;
using System.Windows;
using TheTrader.Domain.Services;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class SearchSymbolCommand : Command
    {
        private readonly BuyViewModel _buyViewModel;
        private readonly IStockPriceService _stockPriceService;


        public SearchSymbolCommand(BuyViewModel buyViewModel, IStockPriceService stockPriceService)
        {
            _buyViewModel = buyViewModel;
            _stockPriceService = stockPriceService;
        }


        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrWhiteSpace(_buyViewModel.Symbol);
        }


        public override async void Execute(object parameter)
        {
            try
            {
                var stockPrice = await _stockPriceService.GetPrice(_buyViewModel.Symbol!);
                _buyViewModel.StockPrice = stockPrice;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
