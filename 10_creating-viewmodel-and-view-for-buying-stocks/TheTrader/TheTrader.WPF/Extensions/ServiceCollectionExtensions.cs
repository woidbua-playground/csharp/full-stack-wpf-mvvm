﻿using Microsoft.Extensions.DependencyInjection;
using TheTrader.WPF.Factories;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddWpfServices(this IServiceCollection services)
        {
            services.AddSingleton<IRootViewModelFactory, RootViewModelFactory>();
            services.AddSingleton<IViewModelFactory<HomeViewModel>, HomeViewModelFactory>();
            services.AddSingleton<IViewModelFactory<PortfolioViewModel>, PortfolioViewModelFactory>();
            services.AddSingleton<IViewModelFactory<MajorIndexListingViewModel>, MajorIndexListingViewModelFactory>();

            services.AddScoped<INavigator, Navigator>();
            services.AddScoped<MainViewModel>();
            services.AddScoped<BuyViewModel>();

            services.AddScoped(sp => new MainWindow(sp.GetRequiredService<MainViewModel>()));
        }
    }
}
