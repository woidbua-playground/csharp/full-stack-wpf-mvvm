﻿using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Factories
{
    public class HomeViewModelFactory : IViewModelFactory<HomeViewModel>
    {
        private readonly IViewModelFactory<MajorIndexListingViewModel> _majorIndexListingViewModelFactory;


        public HomeViewModelFactory(IViewModelFactory<MajorIndexListingViewModel> majorIndexListingViewModelFactory)
        {
            _majorIndexListingViewModelFactory = majorIndexListingViewModelFactory;
        }


        public HomeViewModel Create()
        {
            return new HomeViewModel(_majorIndexListingViewModelFactory.Create());
        }
    }
}
