﻿using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Factories
{
    public interface IViewModelFactory<out T>
        where T : ViewModelBase
    {
        T Create();
    }
}
