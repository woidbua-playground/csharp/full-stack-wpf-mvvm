﻿using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Factories
{
    public class PortfolioViewModelFactory : IViewModelFactory<PortfolioViewModel>
    {
        public PortfolioViewModel Create()
        {
            return new PortfolioViewModel();
        }
    }
}
