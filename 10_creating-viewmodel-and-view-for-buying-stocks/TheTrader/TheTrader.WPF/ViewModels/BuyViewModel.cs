﻿using TheTrader.Domain.Services;
using TheTrader.Domain.Services.TransactionServices;
using TheTrader.WPF.Commands;

namespace TheTrader.WPF.ViewModels
{
    public class BuyViewModel : ViewModelBase
    {
        private string? _symbol;
        private int _sharesToBuy;
        private double _stockPrice;


        public BuyViewModel(IStockPriceService stockPriceService, IBuyStockService buyStockService)
        {
            SearchSymbolCommand = new SearchSymbolCommand(this, stockPriceService);
            BuyStockCommand = new BuyStockCommand(this, buyStockService);

            SharesTuBuy = 1;
        }


        public string? Symbol
        {
            get => _symbol;
            set
            {
                if (_symbol == value)
                    return;

                _symbol = value;
                OnPropertyChanged();
                SearchSymbolCommand.OnCanExecuteChanged();
            }
        }

        public int SharesTuBuy
        {
            get => _sharesToBuy;
            set
            {
                if (_sharesToBuy == value)
                    return;

                _sharesToBuy = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(TotalPrice));
                BuyStockCommand.OnCanExecuteChanged();
            }
        }

        public double StockPrice
        {
            get => _stockPrice;
            set
            {
                if (Equals(_stockPrice, value))
                    return;

                _stockPrice = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(TotalPrice));
                BuyStockCommand.OnCanExecuteChanged();
            }
        }

        public double TotalPrice => SharesTuBuy * StockPrice;

        public Command SearchSymbolCommand { get; }

        public Command BuyStockCommand { get; }
    }
}
