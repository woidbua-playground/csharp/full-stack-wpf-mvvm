﻿namespace TheTrader.Domain.Factories
{
    public interface IFactory<out T>
    {
        T Create();
    }
}
