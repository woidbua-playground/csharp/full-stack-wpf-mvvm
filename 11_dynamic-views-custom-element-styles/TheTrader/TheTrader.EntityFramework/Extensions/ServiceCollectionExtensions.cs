﻿using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services;
using TheTrader.EntityFramework.Services;

namespace TheTrader.EntityFramework.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddEntityFrameworkServices(this IServiceCollection services)
        {
            services.AddSingleton<TheTraderDbContextFactory>();
            services.AddSingleton<IDataService<Account>, AccountDataService>();
        }
    }
}
