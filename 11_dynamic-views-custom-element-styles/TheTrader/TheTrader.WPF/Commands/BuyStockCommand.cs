﻿using System;
using System.Collections.Generic;
using System.Windows;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services.TransactionServices;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class BuyStockCommand : Command
    {
        private readonly BuyViewModel _buyViewModel;
        private readonly IBuyStockService _buyStockService;


        public BuyStockCommand(BuyViewModel buyViewModel, IBuyStockService buyStockService)
        {
            _buyViewModel = buyViewModel;
            _buyStockService = buyStockService;
        }


        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrWhiteSpace(_buyViewModel.Symbol) && _buyViewModel.TotalPrice > 0;
        }


        public override async void Execute(object parameter)
        {
            try
            {
                var account = await _buyStockService.BuyStock(
                    new Account
                    {
                        Id = 1,
                        Balance = 500,
                        AssetTransactions = new List<AssetTransaction>()
                    },
                    _buyViewModel.Symbol!,
                    _buyViewModel.SharesToBuy
                );
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
