﻿using System;
using TheTrader.Domain.Services;
using TheTrader.Domain.Services.TransactionServices;
using TheTrader.WPF.Commands;

namespace TheTrader.WPF.ViewModels
{
    public class BuyViewModel : ViewModelBase
    {
        private string _symbol = string.Empty;
        private string _searchResultSymbol = string.Empty;
        private int _sharesToBuy;
        private double _stockPrice;


        public BuyViewModel(IStockPriceService stockPriceService, IBuyStockService buyStockService)
        {
            SearchSymbolCommand = new SearchSymbolCommand(this, stockPriceService);
            BuyStockCommand = new BuyStockCommand(this, buyStockService);

            SharesToBuy = 1;
        }


        public string Symbol
        {
            get => _symbol;
            set
            {
                if (_symbol == value)
                    return;

                _symbol = value;
                OnPropertyChanged();
                SearchSymbolCommand.OnCanExecuteChanged();
            }
        }

        public string SearchResultSymbol
        {
            get => _searchResultSymbol;
            set
            {
                if (_searchResultSymbol == value)
                    return;

                _searchResultSymbol = value;
                OnPropertyChanged();
            }
        }

        public int SharesToBuy
        {
            get => _sharesToBuy;
            set
            {
                if (_sharesToBuy == value)
                    return;

                _sharesToBuy = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(TotalPrice));
                BuyStockCommand.OnCanExecuteChanged();
            }
        }

        public double StockPrice
        {
            get => _stockPrice;
            set
            {
                if (Math.Abs(_stockPrice - value) < 0.001)
                    return;

                _stockPrice = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(TotalPrice));
                BuyStockCommand.OnCanExecuteChanged();
            }
        }

        public double TotalPrice => SharesToBuy * StockPrice;

        public Command SearchSymbolCommand { get; }

        public Command BuyStockCommand { get; }
    }
}
