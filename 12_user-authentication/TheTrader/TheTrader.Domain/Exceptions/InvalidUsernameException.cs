﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace TheTrader.Domain.Exceptions
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class InvalidUsernameException : Exception
    {
        public InvalidUsernameException(string username)
        {
            Username = username;
        }


        public InvalidUsernameException(string username, string message)
            : base(message)
        {
            Username = username;
        }


        public InvalidUsernameException(string username, string message, Exception innerException)
            : base(message, innerException)
        {
            Username = username;
        }


        public string Username { get; }
    }
}
