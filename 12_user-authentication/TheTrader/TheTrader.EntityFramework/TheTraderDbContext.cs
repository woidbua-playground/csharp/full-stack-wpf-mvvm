﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using TheTrader.Domain.Models;

#pragma warning disable 8618
namespace TheTrader.EntityFramework
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class TheTraderDbContext : DbContext
    {
        public TheTraderDbContext(DbContextOptions options)
            : base(options)
        {
        }


        public DbSet<User> Users { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<AssetTransaction> AssetTransactions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AssetTransaction>().OwnsOne(at => at.Asset);

            base.OnModelCreating(modelBuilder);
        }
    }
}
