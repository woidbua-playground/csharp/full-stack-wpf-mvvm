﻿using System.Collections.Generic;
using TheTrader.WPF.Commands;
using TheTrader.WPF.Factories;
using TheTrader.WPF.Models;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.State.Navigators
{
    public class Navigator : ObservableObject, INavigator
    {
        private ViewModelBase? _currentViewModel;


        public Navigator(IRootViewModelFactory rootViewModelFactory)
        {
            UpdateCurrentViewModelCommand = new UpdateCurrentViewModelCommand(this, rootViewModelFactory);
        }


        public IDictionary<ViewType, ViewModelBase> LoadedViewModels { get; } =
            new Dictionary<ViewType, ViewModelBase>();

        public ViewModelBase? CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                if (_currentViewModel == value)
                    return;

                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

        public Command UpdateCurrentViewModelCommand { get; }
    }
}
