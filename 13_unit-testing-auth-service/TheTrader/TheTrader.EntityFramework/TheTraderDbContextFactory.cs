﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace TheTrader.EntityFramework
{
    public class TheTraderDbContextFactory : IDesignTimeDbContextFactory<TheTraderDbContext>
    {
        private const string ConnectionString =
            @"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=TheTrader;Integrated Security=True;";


        public TheTraderDbContext CreateDbContext(string[]? args = null)
        {
            var options = new DbContextOptionsBuilder<TheTraderDbContext>().UseSqlServer(ConnectionString).Options;
            return new TheTraderDbContext(options);
        }
    }
}
