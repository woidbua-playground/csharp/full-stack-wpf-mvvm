﻿using TheTrader.WPF.Factories;
using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.Commands
{
    public class UpdateCurrentViewModelCommand : Command
    {
        private readonly INavigator _navigator;
        private readonly IRootViewModelFactory _rootViewModelFactory;


        public UpdateCurrentViewModelCommand(INavigator navigator, IRootViewModelFactory rootViewModelFactory)
        {
            _navigator = navigator;
            _rootViewModelFactory = rootViewModelFactory;
        }


        public override bool CanExecute(object parameter)
        {
            return true;
        }


        public override void Execute(object parameter)
        {
            if (!(parameter is ViewType viewType))
                return;

            _navigator.CurrentViewModel = _rootViewModelFactory.CreateViewModel(viewType);
        }
    }
}
