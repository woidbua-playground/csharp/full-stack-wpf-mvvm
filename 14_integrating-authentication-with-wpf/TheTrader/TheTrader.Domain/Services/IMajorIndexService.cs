﻿using System.Threading.Tasks;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services
{
    public interface IMajorIndexService
    {
        Task<MajorIndex> GetMajorIndex(MajorIndexType indexType);
    }
}
