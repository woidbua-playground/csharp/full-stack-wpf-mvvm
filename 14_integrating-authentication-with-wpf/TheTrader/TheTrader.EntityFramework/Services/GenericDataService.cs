﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services;

namespace TheTrader.EntityFramework.Services
{
    public abstract class GenericDataService<T> : IDataService<T>
        where T : DomainObject
    {
        protected GenericDataService(TheTraderDbContextFactory contextFactory)
        {
            ContextFactory = contextFactory;
        }


        protected TheTraderDbContextFactory ContextFactory { get; }


        public async Task<IEnumerable<T>> GetAll()
        {
            await using var context = ContextFactory.CreateDbContext();

            IEnumerable<T> entities = GetDbSetQueryable(context);

            return entities;
        }


        public async Task<T?> Get(int id)
        {
            await using var context = ContextFactory.CreateDbContext();

            var entity = await GetDbSetQueryable(context).FirstOrDefaultAsync((e) => e.Id == id);

            return entity;
        }


        public async Task<T> Create(T entity)
        {
            await using var context = ContextFactory.CreateDbContext();

            var createdResult = await context.Set<T>().AddAsync(entity);
            await context.SaveChangesAsync();

            return createdResult.Entity;
        }


        public async Task<T> Update(int id, T entity)
        {
            await using var context = ContextFactory.CreateDbContext();

            entity.Id = id;
            var updatedResult = context.Set<T>().Update(entity);
            await context.SaveChangesAsync();

            return updatedResult.Entity;
        }


        public async Task<bool> Delete(int id)
        {
            await using var context = ContextFactory.CreateDbContext();

            T entity = await context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
            if (entity == null)
                return false;

            context.Remove(entity);
            await context.SaveChangesAsync();

            return true;
        }


        protected abstract IQueryable<T> GetDbSetQueryable(TheTraderDbContext context);
    }
}
