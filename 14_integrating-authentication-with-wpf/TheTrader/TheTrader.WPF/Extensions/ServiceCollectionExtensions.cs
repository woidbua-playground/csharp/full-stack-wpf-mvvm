﻿using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.State.Authenticators;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels;
using TheTrader.WPF.ViewModels.Factories;

namespace TheTrader.WPF.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddWpfServices(this IServiceCollection services)
        {
            services.AddSingleton<IRootViewModelFactory, RootViewModelFactory>();
            services.AddSingleton<IViewModelFactory<LoginViewModel>, LoginViewModelFactory>();
            services.AddSingleton<IViewModelFactory<HomeViewModel>, HomeViewModelFactory>();
            services.AddSingleton<IViewModelFactory<PortfolioViewModel>, PortfolioViewModelFactory>();
            services.AddSingleton<IViewModelFactory<MajorIndexListingViewModel>, MajorIndexListingViewModelFactory>();

            services.AddScoped<INavigator, Navigator>();
            services.AddScoped<IAuthenticator, Authenticator>();
            services.AddScoped<MainViewModel>();
            services.AddScoped<BuyViewModel>();

            services.AddScoped(sp => new MainWindow(sp.GetRequiredService<MainViewModel>()));
        }
    }
}
