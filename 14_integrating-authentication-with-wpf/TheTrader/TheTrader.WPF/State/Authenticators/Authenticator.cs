﻿using System;
using System.Threading.Tasks;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services.Authentications;
using TheTrader.Domain.State.Authenticators;

namespace TheTrader.WPF.State.Authenticators
{
    public class Authenticator : IAuthenticator
    {
        private readonly IAuthenticationService _authenticationService;


        public Authenticator(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }


        public Account? CurrentAccount { get; private set; }

        public bool IsLoggedIn => CurrentAccount != null;


        public async Task<RegistrationResult> Register(
            string email,
            string username,
            string password,
            string confirmPassword
        )
        {
            var result = await _authenticationService.Register(email, username, password, confirmPassword);

            if (result == RegistrationResult.Success)
                await Login(username, password);

            return result;
        }


        public async Task<bool> Login(string username, string password)
        {
            try
            {
                CurrentAccount = await _authenticationService.Login(username, password);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public void Logout()
        {
            CurrentAccount = null;
        }
    }
}
