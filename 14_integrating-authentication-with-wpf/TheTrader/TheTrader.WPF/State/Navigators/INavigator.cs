﻿using TheTrader.WPF.Commands;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.State.Navigators
{
    public interface INavigator
    {
        ViewModelBase? CurrentViewModel { get; set; }

        Command UpdateCurrentViewModelCommand { get; }
    }
}
