﻿using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.ViewModels.Factories
{
    public interface IRootViewModelFactory
    {
        ViewModelBase CreateViewModel(ViewType viewType);
    }
}
