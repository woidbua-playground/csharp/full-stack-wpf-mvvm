﻿namespace TheTrader.WPF.ViewModels.Factories
{
    public interface IViewModelFactory<out T>
        where T : ViewModelBase
    {
        T Create();
    }
}
