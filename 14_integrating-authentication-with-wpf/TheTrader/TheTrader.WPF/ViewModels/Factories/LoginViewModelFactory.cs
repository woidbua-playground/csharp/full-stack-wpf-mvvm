﻿using TheTrader.Domain.State.Authenticators;

namespace TheTrader.WPF.ViewModels.Factories
{
    public class LoginViewModelFactory : IViewModelFactory<LoginViewModel>
    {
        private readonly IAuthenticator _authenticator;


        public LoginViewModelFactory(IAuthenticator authenticator)
        {
            _authenticator = authenticator;
        }


        public LoginViewModel Create()
        {
            return new LoginViewModel(_authenticator);
        }
    }
}
