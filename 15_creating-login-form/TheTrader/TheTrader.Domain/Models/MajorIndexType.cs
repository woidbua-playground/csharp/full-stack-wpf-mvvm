﻿using System.ComponentModel;

namespace TheTrader.Domain.Models
{
    public enum MajorIndexType
    {
        [Description("Dow Jones")]
        DowJones,

        [Description("NASDAQ")]
        Nasdaq,

        [Description("S&P 500")]
        Sp500
    }
}
