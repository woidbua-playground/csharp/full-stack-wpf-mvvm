﻿using System.Threading.Tasks;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services.Authentications
{
    public interface IAuthenticationService
    {
        Task<RegistrationResult> Register(string email, string username, string password, string confirmPassword);

        Task<Account> Login(string username, string password);
    }
}
