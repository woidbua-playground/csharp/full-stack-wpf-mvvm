﻿using System.Threading.Tasks;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services
{
    public interface IAccountService : IDataService<Account>
    {
        Task<Account?> GetByUsername(string username);

        Task<Account?> GetByEmail(string email);
    }
}
