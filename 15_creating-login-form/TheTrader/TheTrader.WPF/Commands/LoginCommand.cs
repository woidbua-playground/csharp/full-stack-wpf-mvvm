﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class LoginCommand : Command
    {
        private readonly LoginViewModel _loginViewModel;
        private readonly IAuthenticator _authenticator;


        public LoginCommand(LoginViewModel loginViewModel, IAuthenticator authenticator)
        {
            _loginViewModel = loginViewModel;
            _authenticator = authenticator;
        }


        public override bool CanExecute(object parameter)
        {
            return true;
        }


        public override async void Execute(object parameter)
        {
            var password = parameter.ToString() ?? string.Empty;
            await _authenticator.Login(_loginViewModel.Username, password);
        }
    }
}
