﻿using TheTrader.WPF.Commands;
using TheTrader.WPF.Models;
using TheTrader.WPF.ViewModels;
using TheTrader.WPF.ViewModels.Factories;

namespace TheTrader.WPF.State.Navigators
{
    public class Navigator : ObservableObject, INavigator
    {
        private ViewModelBase? _currentViewModel;


        public Navigator(IRootViewModelFactory rootViewModelFactory)
        {
            UpdateCurrentViewModelCommand = new UpdateCurrentViewModelCommand(this, rootViewModelFactory);
        }


        public ViewModelBase? CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                if (_currentViewModel == value)
                    return;

                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

        public Command UpdateCurrentViewModelCommand { get; }
    }
}
