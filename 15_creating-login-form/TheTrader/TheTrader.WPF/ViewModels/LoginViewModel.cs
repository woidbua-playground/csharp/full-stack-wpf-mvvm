﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.Commands;

namespace TheTrader.WPF.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private string _username = string.Empty;


        public LoginViewModel(IAuthenticator authenticator)
        {
            LoginCommand = new LoginCommand(this, authenticator);
        }


        public string Username
        {
            get => _username;
            set
            {
                if (_username == value)
                    return;

                _username = value;
                OnPropertyChanged();
            }
        }

        public Command LoginCommand { get; }
    }
}
