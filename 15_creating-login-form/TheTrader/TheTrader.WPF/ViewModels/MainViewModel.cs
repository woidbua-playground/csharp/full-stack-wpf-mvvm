﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel(IAuthenticator authenticator, INavigator navigator)
        {
            Authenticator = authenticator;

            Navigator = navigator;
            Navigator.UpdateCurrentViewModelCommand.Execute(ViewType.Login);
        }


        public IAuthenticator Authenticator { get; }

        public INavigator Navigator { get; }
    }
}
