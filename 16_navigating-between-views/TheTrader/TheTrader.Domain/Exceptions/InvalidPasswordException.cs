﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace TheTrader.Domain.Exceptions
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class InvalidPasswordException : Exception
    {
        public InvalidPasswordException(string username)
        {
            Username = username;
        }


        public InvalidPasswordException(string username, string message)
            : base(message)
        {
            Username = username;
        }


        public InvalidPasswordException(string username, string message, Exception innerException)
            : base(message, innerException)
        {
            Username = username;
        }


        public string Username { get; }
    }
}
