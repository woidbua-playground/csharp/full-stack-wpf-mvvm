﻿using System.Threading.Tasks;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services.Authentications;

namespace TheTrader.Domain.State.Authenticators
{
    public interface IAuthenticator
    {
        Account? CurrentAccount { get; }

        bool IsLoggedIn { get; }

        Task<RegistrationResult> Register(string email, string username, string password, string confirmPassword);

        Task<bool> Login(string username, string password);

        void Logout();
    }
}
