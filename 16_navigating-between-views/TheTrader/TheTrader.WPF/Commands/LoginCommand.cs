﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class LoginCommand : Command
    {
        private readonly LoginViewModel _loginViewModel;
        private readonly IAuthenticator _authenticator;
        private readonly IRenavigator _renavigator;


        public LoginCommand(LoginViewModel loginViewModel, IAuthenticator authenticator, IRenavigator renavigator)
        {
            _loginViewModel = loginViewModel;
            _authenticator = authenticator;
            _renavigator = renavigator;
        }


        public override bool CanExecute(object parameter)
        {
            return true;
        }


        public override async void Execute(object parameter)
        {
            var password = parameter.ToString() ?? string.Empty;
            var success = await _authenticator.Login(_loginViewModel.Username, password);
            if (success)
                _renavigator.Renavigate();
        }
    }
}
