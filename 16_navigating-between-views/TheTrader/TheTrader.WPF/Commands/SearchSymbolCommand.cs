﻿using System;
using System.Windows;
using TheTrader.Domain.Services;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class SearchSymbolCommand : Command
    {
        private readonly BuyViewModel _buyViewModel;
        private readonly IStockPriceService _stockPriceService;


        public SearchSymbolCommand(BuyViewModel buyViewModel, IStockPriceService stockPriceService)
        {
            _buyViewModel = buyViewModel;
            _stockPriceService = stockPriceService;
        }


        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrWhiteSpace(_buyViewModel.Symbol);
        }


        public override async void Execute(object parameter)
        {
            try
            {
                var uppercaseSymbol = _buyViewModel.Symbol.ToUpper();
                var stockPrice = await _stockPriceService.GetPrice(uppercaseSymbol);
                _buyViewModel.StockPrice = stockPrice;
                _buyViewModel.SearchResultSymbol = uppercaseSymbol;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
