﻿using TheTrader.WPF.Models;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.State.Navigators
{
    public class Navigator : ObservableObject, INavigator
    {
        private ViewModelBase? _currentViewModel;

        public ViewModelBase? CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                if (_currentViewModel == value)
                    return;

                _currentViewModel = value;
                OnPropertyChanged();
            }
        }
    }
}
