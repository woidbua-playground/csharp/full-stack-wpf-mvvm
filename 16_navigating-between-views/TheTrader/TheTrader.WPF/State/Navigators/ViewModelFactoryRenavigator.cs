﻿using TheTrader.WPF.ViewModels;
using TheTrader.WPF.ViewModels.Factories;

namespace TheTrader.WPF.State.Navigators
{
    internal class ViewModelFactoryRenavigator<T> : IRenavigator
        where T : ViewModelBase
    {
        private readonly INavigator _navigator;
        private readonly IViewModelFactory<T> _viewModelFactory;


        public ViewModelFactoryRenavigator(INavigator navigator, IViewModelFactory<T> viewModelFactory)
        {
            _navigator = navigator;
            _viewModelFactory = viewModelFactory;
        }


        public void Renavigate()
        {
            _navigator.CurrentViewModel = _viewModelFactory.Create();
        }
    }
}