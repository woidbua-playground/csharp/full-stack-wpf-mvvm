﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.ViewModels.Factories
{
    public class LoginViewModelFactory : IViewModelFactory<LoginViewModel>
    {
        private readonly IAuthenticator _authenticator;
        private readonly IRenavigator _renavigator;


        public LoginViewModelFactory(IAuthenticator authenticator, IRenavigator renavigator)
        {
            _authenticator = authenticator;
            _renavigator = renavigator;
        }


        public LoginViewModel Create()
        {
            return new LoginViewModel(_authenticator, _renavigator);
        }
    }
}
