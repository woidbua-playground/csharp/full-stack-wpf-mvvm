﻿using TheTrader.Domain.Services;

namespace TheTrader.WPF.ViewModels.Factories
{
    public class MajorIndexListingViewModelFactory : IViewModelFactory<MajorIndexListingViewModel>
    {
        private readonly IMajorIndexService _majorIndexService;


        public MajorIndexListingViewModelFactory(IMajorIndexService majorIndexService)
        {
            _majorIndexService = majorIndexService;
        }


        public MajorIndexListingViewModel Create()
        {
            return MajorIndexListingViewModel.LoadMajorIndexViewModel(_majorIndexService);
        }
    }
}
