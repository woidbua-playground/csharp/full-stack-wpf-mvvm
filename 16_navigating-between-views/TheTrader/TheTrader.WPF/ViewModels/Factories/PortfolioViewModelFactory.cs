﻿namespace TheTrader.WPF.ViewModels.Factories
{
    public class PortfolioViewModelFactory : IViewModelFactory<PortfolioViewModel>
    {
        public PortfolioViewModel Create()
        {
            return new PortfolioViewModel();
        }
    }
}
