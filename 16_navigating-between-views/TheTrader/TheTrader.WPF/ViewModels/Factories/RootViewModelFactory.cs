﻿using System;
using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.ViewModels.Factories
{
    public class RootViewModelFactory : IRootViewModelFactory
    {
        private readonly IViewModelFactory<LoginViewModel> _loginViewModelFactory;
        private readonly IViewModelFactory<HomeViewModel> _homeViewModelFactory;
        private readonly IViewModelFactory<PortfolioViewModel> _portfolioViewModelFactory;
        private readonly BuyViewModel _buyViewModel;


        public RootViewModelFactory(
            IViewModelFactory<LoginViewModel> loginViewModelFactory,
            IViewModelFactory<HomeViewModel> homeViewModelFactory,
            IViewModelFactory<PortfolioViewModel> portfolioViewModelFactory,
            BuyViewModel buyViewModel
        )
        {
            _loginViewModelFactory = loginViewModelFactory;
            _homeViewModelFactory = homeViewModelFactory;
            _portfolioViewModelFactory = portfolioViewModelFactory;
            _buyViewModel = buyViewModel;
        }


        public ViewModelBase CreateViewModel(ViewType viewType)
        {
            return viewType switch
            {
                ViewType.Login => _loginViewModelFactory.Create(),
                ViewType.Home => _homeViewModelFactory.Create(),
                ViewType.Portfolio => _portfolioViewModelFactory.Create(),
                ViewType.Buy => _buyViewModel,
                _ => throw new ArgumentException("Thew viewType does not have a ViewModel", nameof(viewType))
            };
        }
    }
}
