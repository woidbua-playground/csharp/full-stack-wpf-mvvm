﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.Commands;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels.Factories;

namespace TheTrader.WPF.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel(
            IAuthenticator authenticator,
            INavigator navigator,
            IRootViewModelFactory rootViewModelFactory
        )
        {
            Authenticator = authenticator;
            Navigator = navigator;

            UpdateCurrentViewModelCommand = new UpdateCurrentViewModelCommand(Navigator, rootViewModelFactory);
            UpdateCurrentViewModelCommand.Execute(ViewType.Login);
        }


        public IAuthenticator Authenticator { get; }

        public INavigator Navigator { get; }

        public Command UpdateCurrentViewModelCommand { get; }
    }
}
