﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TheTrader.WPF.Views
{
    /// <summary>
    ///     Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public static readonly DependencyProperty LoginCommandProperty = DependencyProperty.Register(
            "LoginCommand",
            typeof(ICommand),
            typeof(LoginView),
            new PropertyMetadata(null)
        );


        public LoginView()
        {
            InitializeComponent();
        }


        public ICommand? LoginCommand
        {
            get => (ICommand) GetValue(LoginCommandProperty);
            set => SetValue(LoginCommandProperty, value);
        }


        private void Login_Click(object sender, RoutedEventArgs e)
        {
            if (LoginCommand == null)
                return;

            string password = PbPassword.Password;
            LoginCommand.Execute(password);
        }
    }
}
