﻿#pragma warning disable 8618
namespace TheTrader.Domain.Models
{
    public class Asset
    {
        public string Symbol { get; set; }

        public double PricePerShare { get; set; }
    }
}
