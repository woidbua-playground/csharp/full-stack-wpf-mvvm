﻿using System.Threading.Tasks;

namespace TheTrader.Domain.Services
{
    public interface IStockPriceService
    {
        Task<double> GetPrice(string symbol);
    }
}
