﻿using System;
using System.Windows;
using TheTrader.Domain.Services.Transactions;
using TheTrader.WPF.State.Accounts;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class BuyStockCommand : Command
    {
        private readonly BuyViewModel _buyViewModel;
        private readonly IBuyStockService _buyStockService;
        private readonly IAccountStore _accountStore;


        public BuyStockCommand(BuyViewModel buyViewModel, IBuyStockService buyStockService, IAccountStore accountStore)
        {
            _buyViewModel = buyViewModel;
            _buyStockService = buyStockService;
            _accountStore = accountStore;
        }


        public override bool CanExecute(object parameter)
        {
            return _accountStore.CurrentAccount != null
                   && !string.IsNullOrWhiteSpace(_buyViewModel.Symbol)
                   && _buyViewModel.TotalPrice > 0;
        }


        public override async void Execute(object parameter)
        {
            try
            {
                if (_accountStore.CurrentAccount == null)
                    return;

                var account = await _buyStockService.BuyStock(
                    _accountStore.CurrentAccount,
                    _buyViewModel.Symbol,
                    _buyViewModel.SharesToBuy
                );

                _accountStore.CurrentAccount = account;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
