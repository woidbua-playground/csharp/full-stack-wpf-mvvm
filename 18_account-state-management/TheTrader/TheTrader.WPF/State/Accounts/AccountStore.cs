﻿using TheTrader.Domain.Models;

namespace TheTrader.WPF.State.Accounts
{
    public class AccountStore : IAccountStore
    {
        public Account? CurrentAccount { get; set; }
    }
}
