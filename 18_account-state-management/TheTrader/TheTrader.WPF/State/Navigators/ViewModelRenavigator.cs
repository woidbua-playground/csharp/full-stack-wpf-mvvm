﻿using TheTrader.WPF.ViewModels;
using TheTrader.WPF.ViewModels.Delegates;

namespace TheTrader.WPF.State.Navigators
{
    internal class ViewModelRenavigator<T> : IRenavigator
        where T : ViewModelBase
    {
        private readonly INavigator _navigator;
        private readonly CreateViewModel<T> _viewModelCreator;


        public ViewModelRenavigator(INavigator navigator, CreateViewModel<T> viewModelCreator)
        {
            _navigator = navigator;
            _viewModelCreator = viewModelCreator;
        }


        public void Renavigate()
        {
            _navigator.CurrentViewModel = _viewModelCreator();
        }
    }
}
