﻿namespace TheTrader.Domain.Services.Authentications
{
    public enum RegistrationResult
    {
        Success,
        PasswordMismatch,
        EmailAlreadyExists,
        UsernameAlreadyExists
    }
}
