﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.Commands;
using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private string _username = string.Empty;


        public LoginViewModel(IAuthenticator authenticator, IRenavigator renavigator)
        {
            ErrorMessageViewModel = new MessageViewModel();

            LoginCommand = new LoginCommand(this, authenticator, renavigator);
        }


        public string Username
        {
            get => _username;
            set
            {
                if (_username == value)
                    return;

                _username = value;
                OnPropertyChanged();
            }
        }

        public MessageViewModel ErrorMessageViewModel { get; }

        public string ErrorMessage
        {
            set => ErrorMessageViewModel.Message = value;
        }

        public Command LoginCommand { get; }
    }
}
