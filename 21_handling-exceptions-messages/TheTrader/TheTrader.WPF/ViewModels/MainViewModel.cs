﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.Commands;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels.Factories;

namespace TheTrader.WPF.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IAuthenticator _authenticator;
        private readonly INavigator _navigator;


        public MainViewModel(IAuthenticator authenticator, INavigator navigator, IViewModelFactory viewModelFactory)
        {
            _authenticator = authenticator;
            _navigator = navigator;

            _authenticator.StateChanged += Authenticator_StateChanged;
            _navigator.StateChanged += Navigator_StateChanged;

            UpdateCurrentViewModelCommand = new UpdateCurrentViewModelCommand(_navigator, viewModelFactory);
            UpdateCurrentViewModelCommand.Execute(ViewType.Login);
        }


        public Command UpdateCurrentViewModelCommand { get; }

        public bool IsLoggedIn => _authenticator.IsLoggedIn;

        public ViewModelBase? CurrentViewModel => _navigator.CurrentViewModel;


        private void Authenticator_StateChanged()
        {
            OnPropertyChanged(nameof(IsLoggedIn));
        }


        private void Navigator_StateChanged()
        {
            OnPropertyChanged(nameof(CurrentViewModel));
        }
    }
}
