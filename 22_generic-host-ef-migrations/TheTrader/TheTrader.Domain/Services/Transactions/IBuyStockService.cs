﻿using System;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services.Transactions
{
    public interface IBuyStockService
    {
        /// <exception cref="InsufficientFundsException">Thrown if the account has an insufficient balance.</exception>
        /// <exception cref="InvalidSymbolException">Thrown if the purchased symbol is invalid.</exception>
        /// <exception cref="Exception">Thrown if the transaction fails.</exception>
        Task<Account> BuyStock(Account buyer, string symbol, int shares);
    }
}
