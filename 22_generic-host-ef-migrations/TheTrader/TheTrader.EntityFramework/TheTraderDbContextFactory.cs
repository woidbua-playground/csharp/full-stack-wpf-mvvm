﻿using Microsoft.EntityFrameworkCore;

namespace TheTrader.EntityFramework
{
    public class TheTraderDbContextFactory
    {
        private readonly string _connectionString;


        public TheTraderDbContextFactory(string connectionString)
        {
            _connectionString = connectionString;
        }


        public TheTraderDbContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<TheTraderDbContext>().UseSqlServer(_connectionString).Options;
            return new TheTraderDbContext(options);
        }
    }
}
