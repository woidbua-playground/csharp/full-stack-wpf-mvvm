﻿namespace TheTrader.FinancialModelingPrepApi.Results
{
    public class StockPriceResult
    {
        public string? Symbol { get; set; }

        public double Price { get; set; }
    }
}
