﻿using System;
using System.Windows.Input;

namespace TheTrader.WPF.Commands
{
    public abstract class Command : ICommand
    {
        public event EventHandler? CanExecuteChanged;

        public abstract bool CanExecute(object parameter);

        public abstract void Execute(object parameter);


        public void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
