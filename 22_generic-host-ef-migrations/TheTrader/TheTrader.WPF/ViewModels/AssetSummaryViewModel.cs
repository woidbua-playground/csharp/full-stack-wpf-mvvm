﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TheTrader.Domain.Models;
using TheTrader.Domain.State;

namespace TheTrader.WPF.ViewModels
{
    public class AssetSummaryViewModel : ViewModelBase
    {
        private readonly IAssetStore _assetStore;
        private readonly ObservableCollection<AssetViewModel> _topAssets;


        public AssetSummaryViewModel(IAssetStore assetStore)
        {
            _assetStore = assetStore;

            _topAssets = new ObservableCollection<AssetViewModel>();
            _assetStore.StateChanged += AssetStore_StateChanged;
            ResetAssets();
        }


        public double AccountBalance => _assetStore.AccountBalance;

        public IEnumerable<AssetViewModel> TopAssets => _topAssets;


        private void AssetStore_StateChanged()
        {
            OnPropertyChanged(nameof(AccountBalance));
            ResetAssets();
        }


        private void ResetAssets()
        {
            var assetViewModels = _assetStore.AssetTransactions.GroupBy(at => at.Asset.Symbol)
                                             .Select(CalculateShares)
                                             .Where(avm => avm.Shares > 0)
                                             .OrderByDescending(a => a.Shares)
                                             .Take(3);
            _topAssets.Clear();
            foreach (var assetViewModel in assetViewModels)
                _topAssets.Add(assetViewModel);
        }


        private static AssetViewModel CalculateShares(IGrouping<string, AssetTransaction> grouping)
        {
            return new AssetViewModel(grouping.Key, grouping.Sum(at => at.IsPurchase ? at.Shares : -at.Shares));
        }
    }
}
