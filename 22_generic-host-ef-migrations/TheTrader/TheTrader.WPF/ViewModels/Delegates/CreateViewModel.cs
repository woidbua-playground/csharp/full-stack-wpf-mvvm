﻿namespace TheTrader.WPF.ViewModels.Delegates
{
    public delegate T CreateViewModel<out T>()
        where T : ViewModelBase;
}
