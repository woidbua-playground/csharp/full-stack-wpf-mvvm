﻿using System;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels.Delegates;

namespace TheTrader.WPF.ViewModels.Factories
{
    public class ViewModelFactory : IViewModelFactory
    {
        private readonly CreateViewModel<LoginViewModel> _loginViewModelCreator;
        private readonly CreateViewModel<HomeViewModel> _homeViewModelCreator;
        private readonly CreateViewModel<PortfolioViewModel> _portfolioViewModelCreator;
        private readonly CreateViewModel<BuyViewModel> _buyViewModelCreator;


        public ViewModelFactory(
            CreateViewModel<LoginViewModel> loginViewModelCreator,
            CreateViewModel<HomeViewModel> homeViewModelCreator,
            CreateViewModel<PortfolioViewModel> portfolioViewModelCreator,
            CreateViewModel<BuyViewModel> buyViewModelCreator
        )
        {
            _loginViewModelCreator = loginViewModelCreator;
            _homeViewModelCreator = homeViewModelCreator;
            _portfolioViewModelCreator = portfolioViewModelCreator;
            _buyViewModelCreator = buyViewModelCreator;
        }


        public ViewModelBase CreateViewModel(ViewType viewType)
        {
            return viewType switch
            {
                ViewType.Login => _loginViewModelCreator(),
                ViewType.Home => _homeViewModelCreator(),
                ViewType.Portfolio => _portfolioViewModelCreator(),
                ViewType.Buy => _buyViewModelCreator(),
                _ => throw new ArgumentException("Thew viewType does not have a ViewModel", nameof(viewType))
            };
        }
    }
}
