﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Services;
using TheTrader.EntityFramework.Services;

namespace TheTrader.EntityFramework.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddEntityFrameworkServices(this IServiceCollection services, string connectionString)
        {
            // strictly for migration
            services.AddDbContext<TheTraderDbContext>(db => db.UseSqlServer(connectionString));

            services.AddSingleton(new TheTraderDbContextFactory(connectionString));
            services.AddSingleton<IAccountService, AccountService>();
        }
    }
}
