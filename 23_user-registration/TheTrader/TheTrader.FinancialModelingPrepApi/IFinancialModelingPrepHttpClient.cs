﻿using System;
using System.Threading.Tasks;

namespace TheTrader.FinancialModelingPrepApi
{
    public interface IFinancialModelingPrepHttpClient : IDisposable
    {
        Task<T> GetAsync<T>(string uri);
    }
}
