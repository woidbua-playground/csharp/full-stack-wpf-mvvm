﻿using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Services;
using TheTrader.Domain.State;
using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.State.Accounts;
using TheTrader.WPF.State.Assets;
using TheTrader.WPF.State.Authenticators;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels;
using TheTrader.WPF.ViewModels.Delegates;
using TheTrader.WPF.ViewModels.Factories;

namespace TheTrader.WPF.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddWpfServices(this IServiceCollection services)
        {
            services.AddScoped<INavigator, Navigator>();
            services.AddScoped<IAuthenticator, Authenticator>();
            services.AddScoped<IAccountStore, AccountStore>();
            services.AddScoped<IAssetStore, AssetStore>();

            // ViewModel Assets
            services.AddSingleton<ViewModelRenavigator<HomeViewModel>>();
            services.AddSingleton<ViewModelRenavigator<LoginViewModel>>();
            services.AddSingleton<ViewModelRenavigator<RegisterViewModel>>();

            // ViewModels
            services.AddScoped<MainViewModel>();
            services.AddScoped<LoginViewModel>();
            services.AddScoped<HomeViewModel>();
            services.AddScoped(
                c => MajorIndexListingViewModel.LoadMajorIndexViewModel(c.GetRequiredService<IMajorIndexService>())
            );
            services.AddScoped<PortfolioViewModel>();
            services.AddScoped<BuyViewModel>();
            services.AddScoped<AssetSummaryViewModel>();

            // ViewModel Factories
            services.AddSingleton<IViewModelFactory, ViewModelFactory>();
            services.AddSingleton<CreateViewModel<LoginViewModel>>(
                c => () => new LoginViewModel(
                    c.GetRequiredService<IAuthenticator>(),
                    c.GetRequiredService<ViewModelRenavigator<HomeViewModel>>(),
                    c.GetRequiredService<ViewModelRenavigator<RegisterViewModel>>()
                )
            );
            services.AddSingleton<CreateViewModel<RegisterViewModel>>(
                c => () => new RegisterViewModel(
                    c.GetRequiredService<IAuthenticator>(),
                    c.GetRequiredService<ViewModelRenavigator<LoginViewModel>>(),
                    c.GetRequiredService<ViewModelRenavigator<LoginViewModel>>()
                )
            );
            services.AddSingleton<CreateViewModel<HomeViewModel>>(c => c.GetRequiredService<HomeViewModel>);
            services.AddSingleton<CreateViewModel<PortfolioViewModel>>(c => c.GetRequiredService<PortfolioViewModel>);
            services.AddSingleton<CreateViewModel<MajorIndexListingViewModel>>(
                c => c.GetRequiredService<MajorIndexListingViewModel>
            );
            services.AddSingleton<CreateViewModel<BuyViewModel>>(c => c.GetRequiredService<BuyViewModel>);

            // Views
            services.AddScoped(c => new MainWindow(c.GetRequiredService<MainViewModel>()));
        }
    }
}
