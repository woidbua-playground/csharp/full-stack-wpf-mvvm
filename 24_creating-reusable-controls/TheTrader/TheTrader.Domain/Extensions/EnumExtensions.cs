﻿using System.ComponentModel;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Extensions
{
    public static class EnumExtensions
    {
        public static string ToDescriptionString(this MajorIndexType? majorIndexType)
        {
            return majorIndexType.HasValue ? ToDescriptionString((MajorIndexType) majorIndexType) : string.Empty;
        }


        public static string ToDescriptionString(this MajorIndexType majorIndexType)
        {
            var attributes = majorIndexType.GetType()
                                           .GetField(majorIndexType.ToString())
                                           ?.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes is DescriptionAttribute[] descriptionAttributes && descriptionAttributes.Length > 0
                ? descriptionAttributes[0].Description
                : string.Empty;
        }
    }
}
