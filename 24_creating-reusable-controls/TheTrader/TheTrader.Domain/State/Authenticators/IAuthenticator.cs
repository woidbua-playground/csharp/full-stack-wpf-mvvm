﻿using System;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services.Authentications;

namespace TheTrader.Domain.State.Authenticators
{
    public interface IAuthenticator
    {
        Account? CurrentAccount { get; }

        bool IsLoggedIn { get; }

        event Action? StateChanged;

        Task<RegistrationResult> Register(string email, string username, string password, string confirmPassword);


        /// <exception cref="UserNotFoundException">Thrown if the user does not exist.</exception>
        /// <exception cref="InvalidPasswordException">Thrown if the password is invalid.</exception>
        /// <exception cref="Exception">Thrown if the login fails.</exception>
        Task Login(string username, string password);


        void Logout();
    }
}
