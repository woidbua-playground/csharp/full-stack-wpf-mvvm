﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services.Transactions
{
    [TestFixture]
    public class BuyStockServiceTests
    {
        private Mock<IStockPriceService> _mockStockPriceService;
        private Mock<IAccountService> _mockAccountService;

        private BuyStockService _buyStockService;

        [SetUp]
        public void SetUp()
        {
            _mockStockPriceService = new Mock<IStockPriceService>();
            _mockAccountService = new Mock<IAccountService>();

            _buyStockService = new BuyStockService(_mockStockPriceService.Object, _mockAccountService.Object);
        }

        [Test]
        public void BuyStock_WithInsufficientFunds_ThrowsInsufficientFundsExceptionForBalances()
        {
            // Arrange
            const double expectedAccountBalance = 0;
            const double expectedRequiredBalance = 100;
            const int shares = 1;
            var buyer = GenerateAccount(expectedAccountBalance);
            _mockStockPriceService.Setup(s => s.GetPrice(It.IsAny<string>())).ReturnsAsync(expectedRequiredBalance);

            // Act
            Task AsyncTestDelegate() => _buyStockService.BuyStock(buyer, It.IsAny<string>(), shares);

            // Assert
            var exception = Assert.ThrowsAsync<InsufficientFundsException>(AsyncTestDelegate);
            Assert.AreEqual(expectedAccountBalance, exception?.AccountBalance);
            Assert.AreEqual(expectedRequiredBalance, exception?.RequiredBalance);
        }

        [Test]
        public void BuyStock_WithInvalidSymbol_ThrowsInvalidSymbolExceptionForSymbol()
        {
            // Arrange
            const string expectedInvalidSymbol = "bad_symbol";
            _mockStockPriceService.Setup(s => s.GetPrice(expectedInvalidSymbol)).ThrowsAsync(new InvalidSymbolException(expectedInvalidSymbol));

            // Act
            Task AsyncTestDelegate() => _buyStockService.BuyStock(It.IsAny<Account>(), expectedInvalidSymbol, It.IsAny<int>());

            // Assert
            var exception = Assert.ThrowsAsync<InvalidSymbolException>(AsyncTestDelegate);
            Assert.AreEqual(expectedInvalidSymbol, exception?.Symbol);
        }

        [Test]
        public void BuyStock_WithAccountUpdateFailure_ThrowsException()
        {
            // Arrange
            const double balance = 1000;
            const double price = 100;
            var buyer = GenerateAccount(balance);
            _mockStockPriceService.Setup(s => s.GetPrice(It.IsAny<string>())).ReturnsAsync(price);
            _mockAccountService.Setup(s => s.Update(It.IsAny<int>(), It.IsAny<Account>())).ThrowsAsync(new Exception());

            // Act
            Task AsyncTestDelegate() => _buyStockService.BuyStock(buyer, It.IsAny<string>(), 1);

            // Assert
            Assert.ThrowsAsync<Exception>(AsyncTestDelegate);
        }

        [Test]
        public void BuyStock_WithGetPriceFailure_ThrowsException()
        {
            // Arrange
            _mockStockPriceService.Setup(s => s.GetPrice(It.IsAny<string>())).ThrowsAsync(new Exception());

            // Act
            Task AsyncTestDelegate() => _buyStockService.BuyStock(It.IsAny<Account>(), It.IsAny<string>(), It.IsAny<int>());

            // Assert
            Assert.ThrowsAsync<Exception>(AsyncTestDelegate);
        }

        [Test]
        public async Task BuyStock_WithSuccessfulPurchase_ReturnsAccountWithNewTransaction()
        {
            // Arrange
            const int expectedTransactionCount = 1;
            const double balance = 1000;
            const double price = 100;
            var buyer = GenerateAccount(balance);
            _mockStockPriceService.Setup(s => s.GetPrice(It.IsAny<string>())).ReturnsAsync(price);

            // Act
            buyer = await _buyStockService.BuyStock(buyer, It.IsAny<string>(), 1);

            // Assert
            Assert.AreEqual(expectedTransactionCount, buyer.AssetTransactions.Count);
        }

        [Test]
        public async Task BuyStock_WithSuccessfulPurchase_ReturnsAccountWithNewBalance()
        {
            // Arrange
            const int expectedBalance = 0;
            const double balance = 100;
            const double price = 50;
            const int shares = 2;
            var buyer = GenerateAccount(balance);
            _mockStockPriceService.Setup(s => s.GetPrice(It.IsAny<string>())).ReturnsAsync(price);

            // Act
            buyer = await _buyStockService.BuyStock(buyer, It.IsAny<string>(), shares);

            // Assert
            Assert.AreEqual(expectedBalance, buyer.Balance);
        }

        private static Account GenerateAccount(double balance)
        {
            return new Account
            {
                Balance = balance,
                AssetTransactions = new List<AssetTransaction>()
            };
        }
    }
}
