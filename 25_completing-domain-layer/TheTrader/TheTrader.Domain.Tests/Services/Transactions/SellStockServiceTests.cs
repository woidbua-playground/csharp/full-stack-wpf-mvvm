﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services.Transactions
{
    [TestFixture]
    public class SellStockServiceTests
    {
        private Mock<IStockPriceService> _mockStockPriceService;
        private Mock<IAccountService> _mockAccountService;

        private SellStockService _sellStockService;

        [SetUp]
        public void SetUp()
        {
            _mockStockPriceService = new Mock<IStockPriceService>();
            _mockAccountService = new Mock<IAccountService>();

            _sellStockService = new SellStockService(_mockStockPriceService.Object, _mockAccountService.Object);
        }

        [Test]
        public void SellStock_WithInsufficientShares_ThrowsInsufficientSharesException()
        {
            // Arrange
            const string symbol = "T";
            const int shares = 10;
            const int requiredShares = shares * 2;
            var seller = GenerateAccount(symbol, shares);

            // Act
            Task AsyncTestDelegate() => _sellStockService.SellStock(seller, symbol, requiredShares);

            // Assert
            var exception = Assert.ThrowsAsync<InsufficientSharesException>(AsyncTestDelegate);
            Assert.AreEqual(symbol, exception?.Symbol);
            Assert.AreEqual(shares, exception?.AccountShares);
            Assert.AreEqual(requiredShares, exception?.RequiredShares);
        }

        [Test]
        public void SellStock_WithInvalidSymbol_ThrowsInvalidSymbolExceptionForSymbol()
        {
            // Arrange
            const string symbol = "bad_symbol";
            const int shares = 10;
            _mockStockPriceService.Setup(s => s.GetPrice(symbol)).ThrowsAsync(new InvalidSymbolException(symbol));

            var seller = GenerateAccount(symbol, shares);

            // Act
            Task AsyncTestDelegate() => _sellStockService.SellStock(seller, symbol, shares / 2);

            // Assert
            var exception = Assert.ThrowsAsync<InvalidSymbolException>(AsyncTestDelegate);
            Assert.AreEqual(symbol, exception?.Symbol);
        }

        [Test]
        public void SellStock_WithGetPriceFailure_ThrowsException()
        {
            // Arrange
            const int shares = 10;
            var seller = GenerateAccount(It.IsAny<string>(), shares);
            _mockStockPriceService.Setup(s => s.GetPrice(It.IsAny<string>())).ThrowsAsync(new Exception());

            // Act
            Task AsyncTestDelegate() => _sellStockService.SellStock(seller, It.IsAny<string>(), shares / 2);

            // Assert
            Assert.ThrowsAsync<Exception>(AsyncTestDelegate);
        }

        [Test]
        public void SellStock_WithAccountUpdateFailure_ThrowsException()
        {
            // Arrange
            const int shares = 10;
            var seller = GenerateAccount(It.IsAny<string>(), shares);
            _mockAccountService.Setup(s => s.Update(It.IsAny<int>(), It.IsAny<Account>())).ThrowsAsync(new Exception());

            // Act
            Task AsyncTestDelegate() => _sellStockService.SellStock(seller, It.IsAny<string>(), shares / 2);

            // Assert
            Assert.ThrowsAsync<Exception>(AsyncTestDelegate);
        }

        [Test]
        public async Task SellStock_WithSufficientShares_ReturnsAccountWithNewTransaction()
        {
            // Arrange
            const int expectedTransactionCount = 2;
            const int shares = 10;
            var seller = GenerateAccount(It.IsAny<string>(), shares);

            // Act
            seller = await _sellStockService.SellStock(seller, It.IsAny<string>(), shares / 2);

            // Assert
            Assert.AreEqual(expectedTransactionCount, seller.AssetTransactions.Count);
        }

        [Test]
        public async Task SellStock_WithSufficientShares_ReturnsAccountWithNewBalance()
        {
            // Arrange
            const int expectedBalance = 100;
            const int shares = 2;
            const int pricePerShare = expectedBalance / 2;
            _mockStockPriceService.Setup(s => s.GetPrice(It.IsAny<string>())).ReturnsAsync(pricePerShare);

            var seller = GenerateAccount(It.IsAny<string>(), shares);

            // Act
            seller = await _sellStockService.SellStock(seller, It.IsAny<string>(), shares);

            // Assert
            Assert.AreEqual(expectedBalance, seller.Balance);
        }

        private static Account GenerateAccount(string symbol, int shares)
        {
            return new Account
            {
                AssetTransactions = new List<AssetTransaction>
                {
                    new AssetTransaction { Asset = new Asset { Symbol = symbol }, IsPurchase = true, Shares = shares }
                }
            };
        }
    }
}
