﻿using System;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Services;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class SearchSymbolCommand : AsyncCommandBase
    {
        private readonly BuyViewModel _buyViewModel;
        private readonly IStockPriceService _stockPriceService;


        public SearchSymbolCommand(BuyViewModel buyViewModel, IStockPriceService stockPriceService)
        {
            _buyViewModel = buyViewModel;
            _stockPriceService = stockPriceService;
        }


        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && !string.IsNullOrWhiteSpace(_buyViewModel.Symbol);
        }


        public override async Task ExecuteAsync(object parameter)
        {
            _buyViewModel.ErrorMessage = string.Empty;

            try
            {
                var uppercaseSymbol = _buyViewModel.Symbol.ToUpper();
                var stockPrice = await _stockPriceService.GetPrice(uppercaseSymbol);
                _buyViewModel.StockPrice = stockPrice;
                _buyViewModel.SearchResultSymbol = uppercaseSymbol;
            }
            catch (InvalidSymbolException)
            {
                _buyViewModel.ErrorMessage = "Symbol does not exist.";
            }
            catch (Exception)
            {
                _buyViewModel.ErrorMessage = "Failed to get symbol information.";
            }
        }
    }
}
