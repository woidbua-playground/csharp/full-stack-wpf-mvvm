﻿using System;
using TheTrader.Domain.Models;

namespace TheTrader.WPF.State.Accounts
{
    public interface IAccountStore
    {
        Account? CurrentAccount { get; set; }

        event Action? StateChanged;
    }
}
