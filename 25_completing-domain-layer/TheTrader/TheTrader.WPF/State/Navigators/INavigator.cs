﻿using System;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.State.Navigators
{
    public interface INavigator
    {
        ViewModelBase? CurrentViewModel { get; set; }

        event Action? StateChanged;
    }
}
