﻿using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.ViewModels.Factories
{
    public interface IViewModelFactory
    {
        ViewModelBase CreateViewModel(ViewType viewType);
    }
}
