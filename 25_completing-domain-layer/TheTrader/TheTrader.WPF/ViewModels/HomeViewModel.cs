﻿namespace TheTrader.WPF.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        public HomeViewModel(
            MajorIndexListingViewModel majorIndexListingViewModel,
            AssetSummaryViewModel assetSummaryViewModel
        )
        {
            MajorIndexListingViewModel = majorIndexListingViewModel;
            AssetSummaryViewModel = assetSummaryViewModel;
        }


        public MajorIndexListingViewModel MajorIndexListingViewModel { get; }

        public AssetSummaryViewModel AssetSummaryViewModel { get; }
    }
}
