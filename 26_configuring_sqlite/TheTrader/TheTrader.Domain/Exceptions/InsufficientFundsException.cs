﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace TheTrader.Domain.Exceptions
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class InsufficientFundsException : Exception
    {
        public InsufficientFundsException(double accountBalance, double requiredBalance)
        {
            AccountBalance = accountBalance;
            RequiredBalance = requiredBalance;
        }


        public InsufficientFundsException(double accountBalance, double requiredBalance, string message)
            : base(message)
        {
            AccountBalance = accountBalance;
            RequiredBalance = requiredBalance;
        }


        public InsufficientFundsException(
            double accountBalance,
            double requiredBalance,
            string message,
            Exception innerException
        )
            : base(message, innerException)
        {
            AccountBalance = accountBalance;
            RequiredBalance = requiredBalance;
        }


        public double AccountBalance { get; }

        public double RequiredBalance { get; }
    }
}
