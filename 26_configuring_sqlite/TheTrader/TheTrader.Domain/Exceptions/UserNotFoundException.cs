﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace TheTrader.Domain.Exceptions
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(string username)
        {
            Username = username;
        }


        public UserNotFoundException(string username, string message)
            : base(message)
        {
            Username = username;
        }


        public UserNotFoundException(string username, string message, Exception innerException)
            : base(message, innerException)
        {
            Username = username;
        }


        public string Username { get; }
    }
}
