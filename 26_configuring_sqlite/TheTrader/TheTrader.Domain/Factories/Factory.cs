﻿using System;

namespace TheTrader.Domain.Factories
{
    public class Factory<T> : IFactory<T>
    {
        private readonly Func<T> _func;


        public Factory(Func<T> func)
        {
            _func = func;
        }


        public T Create()
        {
            return _func();
        }
    }
}
