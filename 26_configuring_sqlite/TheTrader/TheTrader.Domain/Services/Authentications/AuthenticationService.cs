﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services.Authentications
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IAccountService _accountService;
        private readonly IPasswordHasher<string> _passwordHasher;


        public AuthenticationService(IAccountService accountService, IPasswordHasher<string> passwordHasher)
        {
            _accountService = accountService;
            _passwordHasher = passwordHasher;
        }


        public async Task<RegistrationResult> Register(
            string email,
            string username,
            string password,
            string confirmPassword
        )
        {
            if (password != confirmPassword)
                return RegistrationResult.PasswordMismatch;

            var accountByEmail = await _accountService.GetByEmail(email);
            if (accountByEmail != null)
                return RegistrationResult.EmailAlreadyExists;

            var accountByUsername = await _accountService.GetByUsername(username);
            if (accountByUsername != null)
                return RegistrationResult.UsernameAlreadyExists;

            var hashedPassword = _passwordHasher.HashPassword(email, password);
            var user = new User
            {
                Email = email,
                Username = username,
                PasswordHash = hashedPassword,
                DateJoined = DateTime.Now
            };

            var account = new Account { AccountHolder = user };
            await _accountService.Create(account);

            return RegistrationResult.Success;
        }


        public async Task<Account> Login(string username, string password)
        {
            var storedAccount = await _accountService.GetByUsername(username);
            if (storedAccount == null)
                throw new UserNotFoundException(username);

            var passwordResult = _passwordHasher.VerifyHashedPassword(
                storedAccount.AccountHolder.Email,
                storedAccount.AccountHolder.PasswordHash,
                password
            );

            if (passwordResult != PasswordVerificationResult.Success)
                throw new InvalidPasswordException(username);

            return storedAccount;
        }
    }
}
