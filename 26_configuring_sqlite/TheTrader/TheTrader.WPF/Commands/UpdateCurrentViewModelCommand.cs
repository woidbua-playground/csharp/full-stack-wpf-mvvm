﻿using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels.Factories;

namespace TheTrader.WPF.Commands
{
    public class UpdateCurrentViewModelCommand : Command
    {
        private readonly INavigator _navigator;
        private readonly IViewModelFactory _viewModelFactory;


        public UpdateCurrentViewModelCommand(INavigator navigator, IViewModelFactory viewModelFactory)
        {
            _navigator = navigator;
            _viewModelFactory = viewModelFactory;
        }


        public override bool CanExecute(object parameter)
        {
            return true;
        }


        public override void Execute(object parameter)
        {
            if (!(parameter is ViewType viewType))
                return;

            _navigator.CurrentViewModel = _viewModelFactory.CreateViewModel(viewType);
        }
    }
}
