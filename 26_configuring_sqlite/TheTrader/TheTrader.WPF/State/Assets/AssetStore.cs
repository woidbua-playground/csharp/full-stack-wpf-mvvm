﻿using System;
using System.Collections.Generic;
using TheTrader.Domain.Models;
using TheTrader.Domain.State;
using TheTrader.WPF.State.Accounts;

namespace TheTrader.WPF.State.Assets
{
    public class AssetStore : IAssetStore
    {
        private readonly IAccountStore _accountStore;


        public AssetStore(IAccountStore accountStore)
        {
            _accountStore = accountStore;
            _accountStore.StateChanged += OnAccountStoreStateChanged;
        }


        public double AccountBalance => _accountStore.CurrentAccount?.Balance ?? 0;

        public IEnumerable<AssetTransaction> AssetTransactions =>
            _accountStore.CurrentAccount?.AssetTransactions ?? new List<AssetTransaction>();

        public event Action? StateChanged;


        private void OnAccountStoreStateChanged()
        {
            StateChanged?.Invoke();
        }
    }
}
