﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TheTrader.Domain.Models;
using TheTrader.Domain.State;
using TheTrader.WPF.Filters;

namespace TheTrader.WPF.ViewModels
{
    public class AssetListingViewModel : ViewModelBase
    {
        private readonly IAssetStore _assetStore;
        private readonly IAssetsFilter _assetsFilter;
        private readonly ObservableCollection<AssetViewModel> _assets;


        public AssetListingViewModel(IAssetStore assetStore, IAssetsFilter assetsFilter)
        {
            _assetStore = assetStore;
            _assetsFilter = assetsFilter;
            _assets = new ObservableCollection<AssetViewModel>();
            _assetStore.StateChanged += AssetStore_StateChanged;
            ResetAssets();
        }


        public IEnumerable<AssetViewModel> Assets => _assets;


        private void AssetStore_StateChanged()
        {
            ResetAssets();
        }


        private void ResetAssets()
        {
            var assetViewModels = _assetStore.AssetTransactions.GroupBy(at => at.Asset.Symbol)
                                             .Select(CalculateShares)
                                             .Where(avm => avm.Shares > 0)
                                             .OrderByDescending(a => a.Shares)
                                             .AsEnumerable();

            assetViewModels = _assetsFilter.FilterAssets(assetViewModels);

            _assets.Clear();
            foreach (var assetViewModel in assetViewModels)
                _assets.Add(assetViewModel);
        }


        private static AssetViewModel CalculateShares(IGrouping<string, AssetTransaction> grouping)
        {
            return new AssetViewModel(grouping.Key, grouping.Sum(at => at.IsPurchase ? at.Shares : -at.Shares));
        }
    }
}
