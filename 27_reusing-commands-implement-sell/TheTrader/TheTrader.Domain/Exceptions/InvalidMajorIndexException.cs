﻿using System;
using System.Diagnostics.CodeAnalysis;
using TheTrader.Domain.Models;


namespace TheTrader.Domain.Exceptions
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class InvalidMajorIndexException : Exception
    {
        public InvalidMajorIndexException(MajorIndexType majorIndexType)
        {
            MajorIndexType = majorIndexType;
        }


        public InvalidMajorIndexException(MajorIndexType majorIndexType, string message)
            : base(message)
        {
            MajorIndexType = majorIndexType;
        }


        public InvalidMajorIndexException(MajorIndexType majorIndexType, string message, Exception innerException)
            : base(message, innerException)
        {
            MajorIndexType = majorIndexType;
        }


        public MajorIndexType MajorIndexType { get; }
    }
}
