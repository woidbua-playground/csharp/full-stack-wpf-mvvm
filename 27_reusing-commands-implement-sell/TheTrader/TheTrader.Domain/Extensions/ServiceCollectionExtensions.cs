﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Factories;
using TheTrader.Domain.Services.Authentications;
using TheTrader.Domain.Services.Transactions;

namespace TheTrader.Domain.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<IBuyStockService, BuyStockService>();
            services.AddSingleton<ISellStockService, SellStockService>();
            services.AddSingleton<IAuthenticationService, AuthenticationService>();

            services.AddSingleton<IPasswordHasher<string>, PasswordHasher<string>>();
        }

        public static void AddFactory<TService, TImplementation>(this IServiceCollection services)
            where TService : class where TImplementation : class, TService
        {
            services.AddTransient<TService, TImplementation>();
            services.AddSingleton<Func<TService>>(x => x.GetRequiredService<TService>);
            services.AddSingleton<IFactory<TService>, Factory<TService>>();
        }

        public static void AddFactory<TService, TImplementation>(
            this IServiceCollection services,
            Func<IServiceProvider, TImplementation> implementationFactory
        )
            where TService : class where TImplementation : class, TService
        {
            services.AddTransient<TService>(implementationFactory);
            services.AddSingleton<Func<TService>>(x => x.GetRequiredService<TService>);
            services.AddSingleton<IFactory<TService>, Factory<TService>>();
        }
    }
}
