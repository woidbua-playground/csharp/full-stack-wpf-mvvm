﻿using System.Collections.Generic;

#pragma warning disable 8618
namespace TheTrader.Domain.Models
{
    public class Account : DomainObject
    {
        public User AccountHolder { get; set; }

        public double Balance { get; set; }

        public ICollection<AssetTransaction> AssetTransactions { get; set; }
    }
}
