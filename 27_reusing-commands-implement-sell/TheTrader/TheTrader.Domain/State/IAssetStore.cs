﻿using System;
using System.Collections.Generic;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.State
{
    public interface IAssetStore
    {
        double AccountBalance { get; }

        IEnumerable<AssetTransaction> AssetTransactions { get; }

        event Action? StateChanged;
    }
}
