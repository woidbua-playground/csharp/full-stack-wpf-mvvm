﻿using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Extensions;
using TheTrader.Domain.Services;
using TheTrader.FinancialModelingPrepApi.Services;

namespace TheTrader.FinancialModelingPrepApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddFinancialModelingPrepApiServices(this IServiceCollection services, string apiKey)
        {
            services.AddFactory<IFinancialModelingPrepHttpClient, FinancialModelingPrepHttpClient>(
                x => new FinancialModelingPrepHttpClient(apiKey)
            );

            services.AddSingleton<IMajorIndexService, MajorIndexService>();
            services.AddSingleton<IStockPriceService, StockPriceService>();
        }
    }
}
