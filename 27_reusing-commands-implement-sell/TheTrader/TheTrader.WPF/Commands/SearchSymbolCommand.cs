﻿using System;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Services;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class SearchSymbolCommand : AsyncCommandBase
    {
        private readonly ISearchSymbolViewModel _searchSymbolViewModel;
        private readonly IStockPriceService _stockPriceService;


        public SearchSymbolCommand(ISearchSymbolViewModel searchSymbolViewModel, IStockPriceService stockPriceService)
        {
            _searchSymbolViewModel = searchSymbolViewModel;
            _stockPriceService = stockPriceService;
        }


        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && !string.IsNullOrWhiteSpace(_searchSymbolViewModel.Symbol);
        }


        public override async Task ExecuteAsync(object parameter)
        {
            _searchSymbolViewModel.ErrorMessage = string.Empty;

            try
            {
                var symbol = _searchSymbolViewModel.Symbol;
                if (string.IsNullOrWhiteSpace(symbol))
                    return;
                var stockPrice = await _stockPriceService.GetPrice(symbol);
                _searchSymbolViewModel.StockPrice = stockPrice;
                _searchSymbolViewModel.SearchResultSymbol = symbol;
            }
            catch (InvalidSymbolException)
            {
                _searchSymbolViewModel.ErrorMessage = "Symbol does not exist.";
            }
            catch (Exception)
            {
                _searchSymbolViewModel.ErrorMessage = "Failed to get symbol information.";
            }
        }
    }
}
