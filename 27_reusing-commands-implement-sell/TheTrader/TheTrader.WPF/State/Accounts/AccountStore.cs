﻿using System;
using TheTrader.Domain.Models;

namespace TheTrader.WPF.State.Accounts
{
    public class AccountStore : IAccountStore
    {
        private Account? _currentAccount;

        public Account? CurrentAccount
        {
            get => _currentAccount;
            set
            {
                if (_currentAccount == value)
                    return;

                _currentAccount = value;
                StateChanged?.Invoke();
            }
        }

        public event Action? StateChanged;
    }
}
