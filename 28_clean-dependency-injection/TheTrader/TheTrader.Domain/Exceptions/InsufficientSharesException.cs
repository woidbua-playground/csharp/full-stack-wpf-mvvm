﻿using System;
using System.Runtime.Serialization;

namespace TheTrader.Domain.Exceptions
{
    public class InsufficientSharesException : Exception
    {
        public InsufficientSharesException(string symbol, int accountShares, int requiredShares)
        {
            Symbol = symbol;
            AccountShares = accountShares;
            RequiredShares = requiredShares;
        }


        protected InsufficientSharesException(
            string symbol,
            int accountShares,
            int requiredShares,
            SerializationInfo info,
            StreamingContext context
        )
            : base(info, context)
        {
            Symbol = symbol;
            AccountShares = accountShares;
            RequiredShares = requiredShares;
        }


        public InsufficientSharesException(string symbol, int accountShares, int requiredShares, string? message)
            : base(message)
        {
            Symbol = symbol;
            AccountShares = accountShares;
            RequiredShares = requiredShares;
        }


        public InsufficientSharesException(
            string symbol,
            int accountShares,
            int requiredShares,
            string? message,
            Exception? innerException
        )
            : base(message, innerException)
        {
            Symbol = symbol;
            AccountShares = accountShares;
            RequiredShares = requiredShares;
        }


        public string Symbol { get; }

        public int AccountShares { get; }

        public int RequiredShares { get; }
    }
}
