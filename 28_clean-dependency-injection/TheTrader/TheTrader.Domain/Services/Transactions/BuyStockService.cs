﻿using System;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services.Transactions
{
    public class BuyStockService : IBuyStockService
    {
        private readonly IStockPriceService _stockPriceService;
        private readonly IAccountService _accountService;


        public BuyStockService(IStockPriceService stockPriceService, IAccountService accountService)
        {
            _stockPriceService = stockPriceService;
            _accountService = accountService;
        }


        public async Task<Account> BuyStock(Account buyer, string symbol, int shares)
        {
            var stockPrice = await _stockPriceService.GetPrice(symbol);
            var transactionPrice = stockPrice * shares;

            if (transactionPrice > buyer.Balance)
                throw new InsufficientFundsException(buyer.Balance, transactionPrice);

            var transaction = new AssetTransaction
            {
                Account = buyer,
                Asset = new Asset { PricePerShare = stockPrice, Symbol = symbol },
                DateProcessed = DateTime.Now,
                Shares = shares,
                IsPurchase = true
            };

            buyer.AssetTransactions.Add(transaction);
            buyer.Balance -= transactionPrice;

            await _accountService.Update(buyer.Id, buyer);

            return buyer;
        }
    }
}
