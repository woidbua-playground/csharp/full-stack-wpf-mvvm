﻿namespace TheTrader.FinancialModelingPrepApi.Models
{
    public class FinancialModelingPrepApiKey
    {
        public FinancialModelingPrepApiKey(string key)
        {
            Key = key;
        }


        public string Key { get; set; }
    }
}
