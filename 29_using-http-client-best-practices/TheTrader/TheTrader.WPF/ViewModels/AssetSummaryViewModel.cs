﻿using TheTrader.Domain.State;
using TheTrader.WPF.Filters;

namespace TheTrader.WPF.ViewModels
{
    public class AssetSummaryViewModel : ViewModelBase
    {
        private readonly IAssetStore _assetStore;


        public AssetSummaryViewModel(IAssetStore assetStore, IAssetsFilter assetsFilter)
        {
            _assetStore = assetStore;

            AssetListingViewModel = new AssetListingViewModel(assetStore, assetsFilter);
            _assetStore.StateChanged += AssetStore_StateChanged;
        }


        public AssetListingViewModel AssetListingViewModel { get; }

        public double AccountBalance => _assetStore.AccountBalance;


        private void AssetStore_StateChanged()
        {
            OnPropertyChanged(nameof(AccountBalance));
        }
    }
}
