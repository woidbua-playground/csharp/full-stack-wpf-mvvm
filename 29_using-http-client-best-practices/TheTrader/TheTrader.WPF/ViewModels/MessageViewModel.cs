﻿namespace TheTrader.WPF.ViewModels
{
    public class MessageViewModel : ViewModelBase
    {
        private string? _message;

        public string? Message
        {
            get => _message;
            set
            {
                if (_message == value)
                    return;

                _message = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(HasMessage));
            }
        }

        public bool HasMessage => !string.IsNullOrWhiteSpace(Message);
    }
}
