﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Moq;
using NUnit.Framework;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Models;

namespace TheTrader.Domain.Services.Authentications
{
    [TestFixture]
    public class AuthenticationServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            _accountServiceMock = new Mock<IAccountService>();
            _passwordHasherMock = new Mock<IPasswordHasher<string>>();
            _sut = new AuthenticationService(_accountServiceMock.Object, _passwordHasherMock.Object);
        }


        private const string ExpectedUsername = "testUser";
        private const string ExpectedPassword = "testPassword";

        private Mock<IAccountService> _accountServiceMock;
        private Mock<IPasswordHasher<string>> _passwordHasherMock;
        private IAuthenticationService _sut;


        [Test]
        public async Task Register_WithNonMatchingPasswords_ReturnsPasswordMismatchResult()
        {
            // Arrange
            const string password = "pw";
            const string confirmPassword = "confirmPw";

            const RegistrationResult expectedResult = RegistrationResult.PasswordMismatch;

            // Act
            var actualResult = await _sut.Register(It.IsAny<string>(), It.IsAny<string>(), password, confirmPassword);

            // Assert
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }


        [Test]
        public async Task Register_WithAlreadyExistingEmail_ReturnsEmailAlreadyExistsResult()
        {
            // Arrange
            const string email = "test@test.com";
            const RegistrationResult expectedResult = RegistrationResult.EmailAlreadyExists;

            _accountServiceMock.Setup(m => m.GetByEmail(email)).ReturnsAsync(new Account());

            // Act
            var actualResult = await _sut.Register(email, It.IsAny<string>(), ExpectedPassword, ExpectedPassword);

            // Assert
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }


        [Test]
        public async Task Register_WithAlreadyExistingUsername_ReturnsUsernameAlreadyExistsResult()
        {
            // Arrange
            const string username = "testUser";
            const RegistrationResult expectedResult = RegistrationResult.UsernameAlreadyExists;

            _accountServiceMock.Setup(m => m.GetByUsername(username)).ReturnsAsync(new Account());

            // Act
            var actualResult = await _sut.Register(It.IsAny<string>(), username, ExpectedPassword, ExpectedPassword);

            // Assert
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }


        [Test]
        public async Task Register_WithNonExistingUserAndMatchingPasswords_ReturnsSuccessResult()
        {
            // Arrange
            const RegistrationResult expectedResult = RegistrationResult.Success;

            // Act
            var actualResult = await _sut.Register(
                It.IsAny<string>(),
                It.IsAny<string>(),
                ExpectedPassword,
                ExpectedPassword
            );

            // Assert
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }


        [Test]
        public async Task Login_WithCorrectPasswordForExistingUsername_ReturnsAccount()
        {
            // Arrange
            _accountServiceMock.Setup(m => m.GetByUsername(ExpectedUsername))
                               .ReturnsAsync(new Account { AccountHolder = new User { Username = ExpectedUsername } });
            _passwordHasherMock
                .Setup(m => m.VerifyHashedPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(PasswordVerificationResult.Success);

            // Act
            var account = await _sut.Login(ExpectedUsername, ExpectedPassword);

            // Assert
            var actualUsername = account.AccountHolder.Username;
            Assert.That(actualUsername, Is.EqualTo(ExpectedUsername));
        }


        [Test]
        public void Login_WithIncorrectPasswordForExistingUsername_ThrowsInvalidPasswordException()
        {
            // Arrange
            _accountServiceMock.Setup(m => m.GetByUsername(ExpectedUsername))
                               .ReturnsAsync(new Account { AccountHolder = new User { Username = ExpectedUsername } });

            _passwordHasherMock
                .Setup(m => m.VerifyHashedPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(PasswordVerificationResult.Failed);

            // Act
            var exception =
                Assert.ThrowsAsync<InvalidPasswordException>(() => _sut.Login(ExpectedUsername, ExpectedPassword));

            // Assert
            var actualUsername = exception.Username;
            Assert.That(actualUsername, Is.EqualTo(ExpectedUsername));
        }


        [Test]
        public void Login_WithNonExistingUsername_ThrowsUserNotFoundException()
        {
            // Arrange
            _passwordHasherMock
                .Setup(m => m.VerifyHashedPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(PasswordVerificationResult.Failed);

            // Act
            var exception =
                Assert.ThrowsAsync<UserNotFoundException>(() => _sut.Login(ExpectedUsername, ExpectedPassword));

            // Assert
            var actualUsername = exception.Username;
            Assert.That(actualUsername, Is.EqualTo(ExpectedUsername));
        }
    }
}
