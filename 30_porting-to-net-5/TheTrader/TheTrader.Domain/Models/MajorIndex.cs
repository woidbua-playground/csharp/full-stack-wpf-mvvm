﻿using TheTrader.Domain.Extensions;

namespace TheTrader.Domain.Models
{
    public class MajorIndex
    {
        public string Name => Type.ToDescriptionString();

        public double Price { get; set; }

        public double Changes { get; set; }

        public MajorIndexType? Type { get; set; }
    }
}
