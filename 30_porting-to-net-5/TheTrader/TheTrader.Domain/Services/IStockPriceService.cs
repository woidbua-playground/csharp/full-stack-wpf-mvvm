﻿using System;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;

namespace TheTrader.Domain.Services
{
    public interface IStockPriceService
    {
        /// <exception cref="InvalidSymbolException">Thrown if symbol does not exist.</exception>
        /// <exception cref="Exception">Thrown if getting the symbol fails.</exception>
        Task<double> GetPrice(string symbol);
    }
}
