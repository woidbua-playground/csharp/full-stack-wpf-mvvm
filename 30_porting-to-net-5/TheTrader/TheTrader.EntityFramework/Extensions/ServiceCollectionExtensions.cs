﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Services;
using TheTrader.EntityFramework.Services;

namespace TheTrader.EntityFramework.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddEntityFrameworkServices(this IServiceCollection services, string connectionString)
        {
            void ConfigureDbContext(DbContextOptionsBuilder db) => db.UseSqlite(connectionString);

            // strictly for migration
            services.AddDbContext<TheTraderDbContext>(ConfigureDbContext);

            services.AddSingleton(new TheTraderDbContextFactory(ConfigureDbContext));
            services.AddSingleton<IAccountService, AccountService>();
        }
    }
}
