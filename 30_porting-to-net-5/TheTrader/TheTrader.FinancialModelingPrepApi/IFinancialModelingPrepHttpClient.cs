﻿using System.Threading.Tasks;

namespace TheTrader.FinancialModelingPrepApi
{
    public interface IFinancialModelingPrepHttpClient
    {
        Task<T> GetAsync<T>(string uri);
    }
}
