﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services;
using TheTrader.FinancialModelingPrepApi.Results;

namespace TheTrader.FinancialModelingPrepApi.Services
{
    public class MajorIndexService : IMajorIndexService
    {
        private const string BaseUri = "quote";

        private readonly IFinancialModelingPrepHttpClient _httpClient;


        public MajorIndexService(IFinancialModelingPrepHttpClient httpClient)
        {
            _httpClient = httpClient;
        }


        public async Task<MajorIndex> GetMajorIndex(MajorIndexType indexType)
        {
            var uri = $"{BaseUri}/{GetUriSuffix(indexType)}";
            var majorIndexResults = await _httpClient.GetAsync<IList<MajorIndexResult>>(uri);

            if (!majorIndexResults.Any())
                throw new InvalidMajorIndexException(indexType);

            var majorIndexResult = majorIndexResults.First();
            return new MajorIndex
            {
                Changes = majorIndexResult.Change,
                Price = majorIndexResult.Price,
                Type = indexType
            };
        }


        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        private static string GetUriSuffix(MajorIndexType indexType)
        {
            return indexType switch
            {
                MajorIndexType.DowJones => "^DJI",
                MajorIndexType.Nasdaq => "^IXIC",
                MajorIndexType.Sp500 => "^GSPC",
                _ => throw new InvalidMajorIndexException(indexType)
            };
        }
    }
}
