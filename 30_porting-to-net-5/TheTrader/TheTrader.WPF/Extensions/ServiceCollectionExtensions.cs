﻿using System;
using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Services;
using TheTrader.Domain.State;
using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.Filters;
using TheTrader.WPF.State.Accounts;
using TheTrader.WPF.State.Assets;
using TheTrader.WPF.State.Authenticators;
using TheTrader.WPF.State.Navigators;
using TheTrader.WPF.ViewModels;
using TheTrader.WPF.ViewModels.Delegates;
using TheTrader.WPF.ViewModels.Factories;

namespace TheTrader.WPF.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddWpfServices(this IServiceCollection services)
        {
            services.AddScoped<INavigator, Navigator>();
            services.AddScoped<IAuthenticator, Authenticator>();
            services.AddScoped<IAccountStore, AccountStore>();
            services.AddScoped<IAssetStore, AssetStore>();

            // ViewModel Assets
            services.AddSingleton<ViewModelRenavigator<HomeViewModel>>();
            services.AddSingleton<ViewModelRenavigator<LoginViewModel>>();
            services.AddSingleton<ViewModelRenavigator<RegisterViewModel>>();

            // ViewModels
            services.AddScoped<MainViewModel>();
            services.AddScoped<LoginViewModel>();
            services.AddScoped<HomeViewModel>();
            services.AddScoped(CreateMajorIndexListingViewModel);
            services.AddScoped(CreatePortfolioViewModel);
            services.AddScoped<BuyViewModel>();
            services.AddScoped<SellViewModel>();
            services.AddScoped(CreateAssetSummaryViewModel);

            // ViewModel Factories
            services.AddSingleton<IViewModelFactory, ViewModelFactory>();
            services.AddSingleton(CreateLoginViewModelCreator);
            services.AddSingleton(CreateRegisterViewModelCreator);
            services.AddSingleton<CreateViewModel<HomeViewModel>>(c => c.GetRequiredService<HomeViewModel>);
            services.AddSingleton<CreateViewModel<PortfolioViewModel>>(c => c.GetRequiredService<PortfolioViewModel>);
            services.AddSingleton(CreateMajorIndexListingViewModelCreator);
            services.AddSingleton<CreateViewModel<BuyViewModel>>(c => c.GetRequiredService<BuyViewModel>);
            services.AddSingleton<CreateViewModel<SellViewModel>>(c => c.GetRequiredService<SellViewModel>);

            // Views
            services.AddScoped(c => new MainWindow(c.GetRequiredService<MainViewModel>()));
        }


        private static MajorIndexListingViewModel CreateMajorIndexListingViewModel(IServiceProvider serviceProvider)
        {
            return MajorIndexListingViewModel.LoadMajorIndexViewModel(
                serviceProvider.GetRequiredService<IMajorIndexService>()
            );
        }


        private static PortfolioViewModel CreatePortfolioViewModel(IServiceProvider serviceProvider)
        {
            return new PortfolioViewModel(serviceProvider.GetRequiredService<IAssetStore>(), new NonAssetsFilter());
        }


        private static AssetSummaryViewModel CreateAssetSummaryViewModel(IServiceProvider serviceProvider)
        {
            return new AssetSummaryViewModel(serviceProvider.GetRequiredService<IAssetStore>(), new Top3AssetsFilter());
        }


        private static CreateViewModel<LoginViewModel> CreateLoginViewModelCreator(IServiceProvider serviceProvider)
        {
            return () => new LoginViewModel(
                serviceProvider.GetRequiredService<IAuthenticator>(),
                serviceProvider.GetRequiredService<ViewModelRenavigator<HomeViewModel>>(),
                serviceProvider.GetRequiredService<ViewModelRenavigator<RegisterViewModel>>()
            );
        }


        private static CreateViewModel<RegisterViewModel> CreateRegisterViewModelCreator(
            IServiceProvider serviceProvider
        )
        {
            return () => new RegisterViewModel(
                serviceProvider.GetRequiredService<IAuthenticator>(),
                serviceProvider.GetRequiredService<ViewModelRenavigator<LoginViewModel>>(),
                serviceProvider.GetRequiredService<ViewModelRenavigator<LoginViewModel>>()
            );
        }


        private static CreateViewModel<MajorIndexListingViewModel> CreateMajorIndexListingViewModelCreator(
            IServiceProvider serviceProvider
        )
        {
            return serviceProvider.GetRequiredService<MajorIndexListingViewModel>;
        }
    }
}
