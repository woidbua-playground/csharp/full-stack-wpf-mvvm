﻿using System;
using System.Threading.Tasks;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services.Authentications;
using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.State.Accounts;

namespace TheTrader.WPF.State.Authenticators
{
    public class Authenticator : IAuthenticator
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IAccountStore _accountStore;


        public Authenticator(IAuthenticationService authenticationService, IAccountStore accountStore)
        {
            _authenticationService = authenticationService;
            _accountStore = accountStore;
        }


        public Account? CurrentAccount
        {
            get => _accountStore.CurrentAccount;
            private set
            {
                if (_accountStore.CurrentAccount == value)
                    return;

                _accountStore.CurrentAccount = value;
                StateChanged?.Invoke();
            }
        }

        public bool IsLoggedIn => CurrentAccount == null;

        public event Action? StateChanged;


        public async Task<RegistrationResult> Register(
            string email,
            string username,
            string password,
            string confirmPassword
        )
        {
            var result = await _authenticationService.Register(email, username, password, confirmPassword);
            return result;
        }


        public async Task Login(string username, string password)
        {
            CurrentAccount = await _authenticationService.Login(username, password);
        }


        public void Logout()
        {
            CurrentAccount = null;
        }
    }
}
