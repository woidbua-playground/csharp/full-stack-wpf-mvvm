﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TheTrader.WPF.Views
{
    /// <summary>
    ///     Interaction logic for SellView.xaml
    /// </summary>
    public partial class SellView : UserControl
    {
        public static readonly DependencyProperty SelectedAssetChangedCommandProperty = DependencyProperty.Register(
            nameof(SelectedAssetChangedCommand),
            typeof(ICommand),
            typeof(SellView),
            new PropertyMetadata(null)
        );


        public SellView()
        {
            InitializeComponent();
        }


        public ICommand? SelectedAssetChangedCommand
        {
            get => (ICommand?) GetValue(SelectedAssetChangedCommandProperty);
            set => SetValue(SelectedAssetChangedCommandProperty, value);
        }


        private void ComboBoxAssets_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxAssets.SelectedItem != null)
                SelectedAssetChangedCommand?.Execute(null);
        }
    }
}
