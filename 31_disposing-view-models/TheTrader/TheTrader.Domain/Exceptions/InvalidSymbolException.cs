﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace TheTrader.Domain.Exceptions
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class InvalidSymbolException : Exception
    {
        public InvalidSymbolException(string symbol)
        {
            Symbol = symbol;
        }


        public InvalidSymbolException(string symbol, string message)
            : base(message)
        {
            Symbol = symbol;
        }


        public InvalidSymbolException(string symbol, string message, Exception innerException)
            : base(message, innerException)
        {
            Symbol = symbol;
        }


        public string Symbol { get; }
    }
}
