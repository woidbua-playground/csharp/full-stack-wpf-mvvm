﻿using System;
using Microsoft.EntityFrameworkCore;

namespace TheTrader.EntityFramework
{
    public class TheTraderDbContextFactory
    {
        private readonly Action<DbContextOptionsBuilder> _configureDbContext;


        public TheTraderDbContextFactory(Action<DbContextOptionsBuilder> configureDbContext)
        {
            _configureDbContext = configureDbContext;
        }


        public TheTraderDbContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<TheTraderDbContext>();
            _configureDbContext(options);

            return new TheTraderDbContext(options.Options);
        }
    }
}
