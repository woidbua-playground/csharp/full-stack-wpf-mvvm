﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace TheTrader.WPF.Extensions
{
    public static class HostBuilderExtensions
    {
        public static IHostBuilder AddConfiguration(this IHostBuilder hostBuilder)
        {
            return hostBuilder.ConfigureAppConfiguration(
                c =>
                {
                    c.AddJsonFile("appsettings.json");
                    c.AddEnvironmentVariables();
                }
            );
        }
    }
}
