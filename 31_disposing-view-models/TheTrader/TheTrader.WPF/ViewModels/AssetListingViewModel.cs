﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TheTrader.Domain.Models;
using TheTrader.Domain.State;
using TheTrader.WPF.Filters;

namespace TheTrader.WPF.ViewModels
{
    public class AssetListingViewModel : ViewModelBase
    {
        private readonly IAssetStore _assetStore;
        private readonly IAssetsFilter _assetsFilter;
        private readonly ObservableCollection<AssetViewModel> _assets;


        public AssetListingViewModel(IAssetStore assetStore)
            : this(assetStore, new NonAssetsFilter())
        {
        }


        public AssetListingViewModel(IAssetStore assetStore, IAssetsFilter assetsFilter)
        {
            _assetStore = assetStore;
            _assetsFilter = assetsFilter;
            _assets = new ObservableCollection<AssetViewModel>();
            _assetStore.StateChanged += AssetStore_StateChanged;
            ResetAssets();
        }


        public IEnumerable<AssetViewModel> Assets => _assets;


        private void AssetStore_StateChanged()
        {
            ResetAssets();
        }


        private void ResetAssets()
        {
            var assetViewModels = _assetStore.AssetTransactions.GroupBy(at => at.Asset.Symbol)
                                             .Select(CalculateShares)
                                             .Where(avm => avm.Shares > 0)
                                             .OrderByDescending(a => a.Shares)
                                             .AsEnumerable()
                                             .ToList();

            var newAssetViewModels = _assetsFilter.FilterAssets(assetViewModels).ToList();
            var filteredOutAssetViewModels = assetViewModels.Where(avm => !newAssetViewModels.Contains(avm));

            DisposeAssets(filteredOutAssetViewModels);
            DisposeAssets(Assets);
            _assets.Clear();

            foreach (var avm in newAssetViewModels)
                _assets.Add(avm);
        }


        private static AssetViewModel CalculateShares(IGrouping<string, AssetTransaction> grouping)
        {
            return new AssetViewModel(grouping.Key, grouping.Sum(at => at.IsPurchase ? at.Shares : -at.Shares));
        }


        public override void Dispose()
        {
            DisposeAssets(Assets);
            _assetStore.StateChanged -= AssetStore_StateChanged;

            base.Dispose();
        }


        private static void DisposeAssets(IEnumerable<AssetViewModel> assets)
        {
            foreach (var asset in assets)
                asset.Dispose();
        }
    }
}
