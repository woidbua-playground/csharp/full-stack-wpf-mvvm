﻿using System;
using TheTrader.Domain.Services;
using TheTrader.Domain.Services.Transactions;
using TheTrader.WPF.Commands;
using TheTrader.WPF.State.Accounts;

namespace TheTrader.WPF.ViewModels
{
    public class BuyViewModel : ViewModelBase, ISearchSymbolViewModel
    {
        private string? _symbol;
        private string? _searchResultSymbol;
        private int _sharesToBuy;
        private double _stockPrice;


        public BuyViewModel(
            IStockPriceService stockPriceService,
            IBuyStockService buyStockService,
            IAccountStore accountStore
        )
        {
            SearchSymbolCommand = new SearchSymbolCommand(this, stockPriceService);
            BuyStockCommand = new BuyStockCommand(this, buyStockService, accountStore);

            ErrorMessageViewModel = new MessageViewModel();
            StatusMessageViewModel = new MessageViewModel();

            SharesToBuy = 1;
        }


        public int SharesToBuy
        {
            get => _sharesToBuy;
            set
            {
                if (_sharesToBuy == value)
                    return;

                _sharesToBuy = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(TotalPrice));
                BuyStockCommand.OnCanExecuteChanged();
            }
        }

        public double TotalPrice => SharesToBuy * StockPrice;

        public MessageViewModel ErrorMessageViewModel { get; }

        public string? StatusMessage
        {
            set => StatusMessageViewModel.Message = value;
        }

        public MessageViewModel StatusMessageViewModel { get; }

        public AsyncCommandBase SearchSymbolCommand { get; }

        public AsyncCommandBase BuyStockCommand { get; }

        public string? Symbol
        {
            get => _symbol;
            set
            {
                if (_symbol == value)
                    return;

                _symbol = value;
                OnPropertyChanged();
                SearchSymbolCommand.OnCanExecuteChanged();
            }
        }

        public string? SearchResultSymbol
        {
            get => _searchResultSymbol;
            set
            {
                if (_searchResultSymbol == value)
                    return;

                _searchResultSymbol = value;
                OnPropertyChanged();
            }
        }

        public double StockPrice
        {
            get => _stockPrice;
            set
            {
                if (Math.Abs(_stockPrice - value) < 0.001)
                    return;

                _stockPrice = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(TotalPrice));
                BuyStockCommand.OnCanExecuteChanged();
            }
        }

        public string? ErrorMessage
        {
            set => ErrorMessageViewModel.Message = value;
        }


        public override void Dispose()
        {
            ErrorMessageViewModel.Dispose();
            StatusMessageViewModel.Dispose();

            base.Dispose();
        }
    }
}
