﻿using TheTrader.Domain.State.Authenticators;
using TheTrader.WPF.Commands;
using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private string _username = string.Empty;
        private string _password = string.Empty;


        public LoginViewModel(
            IAuthenticator authenticator,
            IRenavigator loginRenavigator,
            IRenavigator registerRenavigator
        )
        {
            ErrorMessageViewModel = new MessageViewModel();

            LoginCommand = new LoginCommand(this, authenticator, loginRenavigator);
            ViewRegisterCommand = new RenavigateCommand(registerRenavigator);
        }


        public string Username
        {
            get => _username;
            set
            {
                if (_username == value)
                    return;

                _username = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (_password == value)
                    return;

                _password = value;
                OnPropertyChanged();
            }
        }

        public MessageViewModel ErrorMessageViewModel { get; }

        public string ErrorMessage
        {
            set => ErrorMessageViewModel.Message = value;
        }

        public AsyncCommandBase LoginCommand { get; }

        public Command ViewRegisterCommand { get; }


        public override void Dispose()
        {
            ErrorMessageViewModel.Dispose();

            base.Dispose();
        }
    }
}
