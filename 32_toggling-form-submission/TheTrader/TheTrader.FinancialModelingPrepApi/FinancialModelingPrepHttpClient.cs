﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TheTrader.FinancialModelingPrepApi.Models;

namespace TheTrader.FinancialModelingPrepApi
{
    public class FinancialModelingPrepHttpClient : IFinancialModelingPrepHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly string _apiKey;


        public FinancialModelingPrepHttpClient(HttpClient httpClient, FinancialModelingPrepApiKey apiKey)
        {
            _httpClient = httpClient;
            _apiKey = apiKey.Key;
        }


        public async Task<T> GetAsync<T>(string uri)
        {
            var response = await _httpClient.GetAsync($"{uri}?apikey={_apiKey}");
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<T>(jsonResponse);
            return result;
        }
    }
}
