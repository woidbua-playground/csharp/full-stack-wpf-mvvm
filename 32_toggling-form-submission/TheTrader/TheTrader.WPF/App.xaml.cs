﻿using System.Windows;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TheTrader.Domain.Extensions;
using TheTrader.EntityFramework;
using TheTrader.EntityFramework.Extensions;
using TheTrader.FinancialModelingPrepApi.Extensions;
using TheTrader.WPF.Extensions;

namespace TheTrader.WPF
{
    public partial class App : Application
    {
        private readonly IHost _host;


        public App()
        {
            _host = CreateHostBuilder().Build();
        }


        public static IHostBuilder CreateHostBuilder(string[]? args = null)
        {
            return Host.CreateDefaultBuilder(args)
                       .AddConfiguration()
                       .ConfigureServices(
                           (context, services) =>
                           {
                               services.AddDomainServices();
                               services.AddEntityFrameworkServices(GetConnectionString(context));
                               services.AddFinancialModelingPrepApiServices(GetFinancialModelingPrepApiKey(context));
                               services.AddWpfServices();
                           }
                       );
        }


        private static string GetConnectionString(HostBuilderContext context)
        {
            return context.Configuration.GetConnectionString("sqLite");
        }


        private static string GetFinancialModelingPrepApiKey(HostBuilderContext context)
        {
            return context.Configuration.GetValue<string>("financeApiKey");
        }


        protected override async void OnStartup(StartupEventArgs e)
        {
            await _host.StartAsync();

            var contextFactory = _host.Services.GetRequiredService<TheTraderDbContextFactory>();
            await using (var context = contextFactory.CreateDbContext())
                await context.Database.MigrateAsync();

            var window = _host.Services.GetRequiredService<MainWindow>();
            window.Show();

            base.OnStartup(e);
        }


        protected override async void OnExit(ExitEventArgs e)
        {
            await _host.StopAsync();
            _host.Dispose();

            base.OnExit(e);
        }
    }
}
