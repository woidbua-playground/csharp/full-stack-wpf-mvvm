﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Services.Transactions;
using TheTrader.WPF.State.Accounts;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class BuyStockCommand : AsyncCommandBase
    {
        private readonly BuyViewModel _buyViewModel;
        private readonly IBuyStockService _buyStockService;
        private readonly IAccountStore _accountStore;


        public BuyStockCommand(BuyViewModel buyViewModel, IBuyStockService buyStockService, IAccountStore accountStore)
        {
            _buyViewModel = buyViewModel;
            _buyStockService = buyStockService;
            _accountStore = accountStore;

            _buyViewModel.PropertyChanged += BuyViewModel_PropertyChanged;
        }


        public override bool CanExecute(object? parameter)
        {
            return _accountStore.CurrentAccount != null && _buyViewModel.CanBuyStock && base.CanExecute(parameter);
        }


        public override async Task ExecuteAsync(object? parameter)
        {
            _buyViewModel.StatusMessage = null;
            _buyViewModel.ErrorMessage = null;

            try
            {
                var symbol = _buyViewModel.Symbol;
                var shares = _buyViewModel.SharesToBuy;

                if (_accountStore.CurrentAccount == null || string.IsNullOrWhiteSpace(symbol))
                    return;

                var account = await _buyStockService.BuyStock(
                    _accountStore.CurrentAccount,
                    symbol,
                    _buyViewModel.SharesToBuy
                );

                _accountStore.CurrentAccount = account;

                _buyViewModel.StatusMessage = $"Successfully purchased {shares} of {symbol}.";
            }
            catch (InsufficientFundsException)
            {
                _buyViewModel.ErrorMessage =
                    "Account has insufficient funds. Please transfer more money into your account.";
            }
            catch (InvalidSymbolException)
            {
                _buyViewModel.ErrorMessage = "Symbol does not exist.";
            }
            catch (Exception)
            {
                _buyViewModel.ErrorMessage = "Transaction failed.";
            }
        }


        private void BuyViewModel_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(BuyViewModel.CanBuyStock))
                OnCanExecuteChanged();
        }
    }
}
