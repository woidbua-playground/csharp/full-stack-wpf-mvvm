﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Services.Transactions;
using TheTrader.WPF.State.Accounts;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class SellStockCommand : AsyncCommandBase
    {
        private readonly SellViewModel _sellViewModel;
        private readonly ISellStockService _sellStockService;
        private readonly IAccountStore _accountStore;


        public SellStockCommand(
            SellViewModel sellViewModel,
            ISellStockService sellStockService,
            IAccountStore accountStore
        )
        {
            _sellViewModel = sellViewModel;
            _sellStockService = sellStockService;
            _accountStore = accountStore;

            _sellViewModel.PropertyChanged += SellViewModel_PropertyChanged;
        }


        public override bool CanExecute(object? parameter)
        {
            return _sellViewModel.CanSellStock && base.CanExecute(parameter);
        }


        public override async Task ExecuteAsync(object? parameter)
        {
            _sellViewModel.StatusMessage = null;
            _sellViewModel.ErrorMessage = null;

            try
            {
                var symbol = _sellViewModel.Symbol;
                var shares = _sellViewModel.SharesToSell;

                if (_accountStore.CurrentAccount == null || string.IsNullOrWhiteSpace(symbol))
                    return;

                var account = await _sellStockService.SellStock(_accountStore.CurrentAccount, symbol, shares);

                _accountStore.CurrentAccount = account;

                _sellViewModel.SearchResultSymbol = null;
                _sellViewModel.StatusMessage = $"Successfully sold {shares} of {symbol}.";
            }
            catch (InsufficientSharesException ex)
            {
                _sellViewModel.ErrorMessage =
                    $"Account has insufficient shares. You only have {ex.AccountShares} shares.";
            }
            catch (InvalidSymbolException)
            {
                _sellViewModel.ErrorMessage = "Symbol does not exist.";
            }
            catch (Exception)
            {
                _sellViewModel.ErrorMessage = "Transaction failed.";
            }
        }


        private void SellViewModel_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SellViewModel.CanSellStock))
                OnCanExecuteChanged();
        }
    }
}
