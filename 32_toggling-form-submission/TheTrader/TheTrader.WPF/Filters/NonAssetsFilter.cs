﻿using System.Collections.Generic;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Filters
{
    public class NonAssetsFilter : IAssetsFilter
    {
        public IEnumerable<AssetViewModel> FilterAssets(IEnumerable<AssetViewModel> assets)
        {
            return assets;
        }
    }
}
