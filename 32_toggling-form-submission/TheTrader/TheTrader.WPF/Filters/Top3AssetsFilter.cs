﻿using System.Collections.Generic;
using System.Linq;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Filters
{
    public class Top3AssetsFilter : IAssetsFilter
    {
        public IEnumerable<AssetViewModel> FilterAssets(IEnumerable<AssetViewModel> assets)
        {
            return assets.Take(3);
        }
    }
}
