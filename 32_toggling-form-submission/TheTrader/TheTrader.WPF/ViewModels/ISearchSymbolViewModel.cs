﻿using System.ComponentModel;

namespace TheTrader.WPF.ViewModels
{
    public interface ISearchSymbolViewModel : INotifyPropertyChanged
    {
        bool CanSearchSymbol { get; }

        string? Symbol { get; }

        string? SearchResultSymbol { set; }

        double StockPrice { set; }

        string? ErrorMessage { set; }
    }
}
