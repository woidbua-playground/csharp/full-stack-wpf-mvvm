﻿using TheTrader.Domain.State;
using TheTrader.WPF.Filters;

namespace TheTrader.WPF.ViewModels
{
    public class PortfolioViewModel : ViewModelBase
    {
        public PortfolioViewModel(IAssetStore assetStore, IAssetsFilter assetsFilter)
        {
            AssetListingViewModel = new AssetListingViewModel(assetStore, assetsFilter);
        }


        public AssetListingViewModel AssetListingViewModel { get; }


        public override void Dispose()
        {
            AssetListingViewModel.Dispose();
            base.Dispose();
        }
    }
}
