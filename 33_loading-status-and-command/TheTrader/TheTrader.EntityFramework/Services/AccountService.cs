﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services;

namespace TheTrader.EntityFramework.Services
{
    public class AccountService : GenericDataService<Account>, IAccountService
    {
        public AccountService(TheTraderDbContextFactory contextFactory)
            : base(contextFactory)
        {
        }


        public async Task<Account?> GetByUsername(string username)
        {
            await using var context = ContextFactory.CreateDbContext();

            return await GetDbSetQueryable(context)
                         .Where(a => a.AccountHolder.Username == username)
                         .FirstOrDefaultAsync();
        }


        public async Task<Account?> GetByEmail(string email)
        {
            await using var context = ContextFactory.CreateDbContext();

            return await GetDbSetQueryable(context).Where(a => a.AccountHolder.Email == email).FirstOrDefaultAsync();
        }


        protected override IQueryable<Account> GetDbSetQueryable(TheTraderDbContext context)
        {
            return context.Accounts.Include(a => a.AssetTransactions).Include(a => a.AccountHolder);
        }
    }
}
