﻿using System;
using Microsoft.Extensions.DependencyInjection;
using TheTrader.Domain.Services;
using TheTrader.FinancialModelingPrepApi.Models;
using TheTrader.FinancialModelingPrepApi.Services;

namespace TheTrader.FinancialModelingPrepApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddFinancialModelingPrepApiServices(this IServiceCollection services, string apiKey)
        {
            services.AddSingleton(new FinancialModelingPrepApiKey(apiKey));
            services.AddHttpClient<IFinancialModelingPrepHttpClient, FinancialModelingPrepHttpClient>(
                c => { c.BaseAddress = new Uri("https://financialmodelingprep.com/api/v3/"); }
            );

            services.AddSingleton<IMajorIndexService, MajorIndexService>();
            services.AddSingleton<IStockPriceService, StockPriceService>();
        }
    }
}
