﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheTrader.Domain.Exceptions;
using TheTrader.Domain.Services;
using TheTrader.FinancialModelingPrepApi.Results;

namespace TheTrader.FinancialModelingPrepApi.Services
{
    public class StockPriceService : IStockPriceService
    {
        private const string BaseUri = "quote-short";

        private readonly IFinancialModelingPrepHttpClient _httpClient;


        public StockPriceService(IFinancialModelingPrepHttpClient httpClient)
        {
            _httpClient = httpClient;
        }


        public async Task<double> GetPrice(string symbol)
        {
            var uri = $"{BaseUri}/{symbol}";

            var stockPriceResults = await _httpClient.GetAsync<IList<StockPriceResult>>(uri);

            if (!stockPriceResults.Any())
                throw new InvalidSymbolException(symbol);

            return stockPriceResults.First().Price;
        }
    }
}
