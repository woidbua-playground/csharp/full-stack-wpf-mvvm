﻿using System.Threading.Tasks;
using TheTrader.Domain.Models;
using TheTrader.Domain.Services;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Commands
{
    public class LoadMajorIndicesCommand : AsyncCommandBase
    {
        private readonly MajorIndexListingViewModel _majorIndexListingViewModel;
        private readonly IMajorIndexService _majorIndexService;


        public LoadMajorIndicesCommand(
            MajorIndexListingViewModel majorIndexListingViewModel,
            IMajorIndexService majorIndexService
        )
        {
            _majorIndexListingViewModel = majorIndexListingViewModel;
            _majorIndexService = majorIndexService;
        }


        public override async Task ExecuteAsync(object? parameter)
        {
            _majorIndexListingViewModel.IsLoading = true;

            await Task.WhenAll(LoadDowJones(), LoadNasdaq(), LoadSp500());

            _majorIndexListingViewModel.IsLoading = false;
        }


        private async Task LoadDowJones()
        {
            _majorIndexListingViewModel.DowJones = await _majorIndexService.GetMajorIndex(MajorIndexType.DowJones);
        }


        private async Task LoadNasdaq()
        {
            _majorIndexListingViewModel.Nasdaq = await _majorIndexService.GetMajorIndex(MajorIndexType.Nasdaq);
        }


        private async Task LoadSp500()
        {
            _majorIndexListingViewModel.Sp500 = await _majorIndexService.GetMajorIndex(MajorIndexType.Sp500);
        }
    }
}
