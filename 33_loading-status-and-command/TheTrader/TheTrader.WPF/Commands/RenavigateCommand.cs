﻿using TheTrader.WPF.State.Navigators;

namespace TheTrader.WPF.Commands
{
    public class RenavigateCommand : Command
    {
        private readonly IRenavigator _renavigator;


        public RenavigateCommand(IRenavigator renavigator)
        {
            _renavigator = renavigator;
        }


        public override bool CanExecute(object parameter)
        {
            return true;
        }


        public override void Execute(object parameter)
        {
            _renavigator.Renavigate();
        }
    }
}
