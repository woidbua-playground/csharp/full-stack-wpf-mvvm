﻿using System.Collections.Generic;
using TheTrader.WPF.ViewModels;

namespace TheTrader.WPF.Filters
{
    public interface IAssetsFilter
    {
        IEnumerable<AssetViewModel> FilterAssets(IEnumerable<AssetViewModel> assets);
    }
}
