﻿using TheTrader.Domain.Models;
using TheTrader.Domain.Services;
using TheTrader.WPF.Commands;

namespace TheTrader.WPF.ViewModels
{
    public class MajorIndexListingViewModel : ViewModelBase
    {
        private bool _isLoading;

        private MajorIndex? _dowJones;
        private MajorIndex? _nasdaq;
        private MajorIndex? _sp500;


        private MajorIndexListingViewModel(IMajorIndexService majorIndexService)
        {
            LoadMajorIndicesCommand = new LoadMajorIndicesCommand(this, majorIndexService);
        }


        public AsyncCommandBase LoadMajorIndicesCommand { get; }

        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                if (_isLoading == value)
                    return;

                _isLoading = value;
                OnPropertyChanged();
            }
        }

        public MajorIndex? DowJones
        {
            get => _dowJones;
            set
            {
                if (_dowJones == value)
                    return;

                _dowJones = value;
                OnPropertyChanged();
            }
        }

        public MajorIndex? Nasdaq
        {
            get => _nasdaq;
            set
            {
                if (_nasdaq == value)
                    return;

                _nasdaq = value;
                OnPropertyChanged();
            }
        }

        public MajorIndex? Sp500
        {
            get => _sp500;
            set
            {
                if (_sp500 == value)
                    return;

                _sp500 = value;
                OnPropertyChanged();
            }
        }


        public static MajorIndexListingViewModel LoadMajorIndexViewModel(IMajorIndexService majorIndexService)
        {
            var majorIndexViewModel = new MajorIndexListingViewModel(majorIndexService);
            majorIndexViewModel.LoadMajorIndicesCommand.Execute(null);
            return majorIndexViewModel;
        }
    }
}
