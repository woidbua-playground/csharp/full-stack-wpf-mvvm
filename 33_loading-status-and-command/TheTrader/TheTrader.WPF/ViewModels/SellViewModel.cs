﻿using System;
using TheTrader.Domain.Services;
using TheTrader.Domain.Services.Transactions;
using TheTrader.Domain.State;
using TheTrader.WPF.Commands;
using TheTrader.WPF.State.Accounts;

namespace TheTrader.WPF.ViewModels
{
    public class SellViewModel : ViewModelBase, ISearchSymbolViewModel
    {
        private AssetViewModel? _selectedAsset;
        private string? _searchResultSymbol;
        private double _stockPrice;
        private int _sharesToSell;


        public SellViewModel(
            IAssetStore assetStore,
            IStockPriceService stockPriceService,
            ISellStockService sellStockService,
            IAccountStore accountStore
        )
        {
            AssetListingViewModel = new AssetListingViewModel(assetStore);

            SearchSymbolCommand = new SearchSymbolCommand(this, stockPriceService);
            SellStockCommand = new SellStockCommand(this, sellStockService, accountStore);

            ErrorMessageViewModel = new MessageViewModel();
            StatusMessageViewModel = new MessageViewModel();
        }


        public AsyncCommandBase SearchSymbolCommand { get; }

        public AsyncCommandBase SellStockCommand { get; }

        public AssetListingViewModel AssetListingViewModel { get; }

        public AssetViewModel? SelectedAsset
        {
            get => _selectedAsset;
            set
            {
                if (_selectedAsset == value)
                    return;

                _selectedAsset = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Symbol));
                OnPropertyChanged(nameof(CanSearchSymbol));
            }
        }

        public MessageViewModel ErrorMessageViewModel { get; }

        public MessageViewModel StatusMessageViewModel { get; }

        public string? StatusMessage
        {
            set => StatusMessageViewModel.Message = value;
        }

        public bool CanSellStock => SharesToSell > 0;

        public int SharesToSell
        {
            get => _sharesToSell;
            set
            {
                if (_sharesToSell == value)
                    return;

                _sharesToSell = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(TotalPrice));
                OnPropertyChanged(nameof(CanSellStock));
            }
        }

        public double TotalPrice => SharesToSell * StockPrice;

        public bool CanSearchSymbol => !string.IsNullOrWhiteSpace(Symbol);

        public string? Symbol => SelectedAsset?.Symbol;

        public string? SearchResultSymbol
        {
            get => _searchResultSymbol;
            set
            {
                if (_searchResultSymbol == value)
                    return;

                _searchResultSymbol = value;
                OnPropertyChanged();
            }
        }

        public double StockPrice
        {
            get => _stockPrice;
            set
            {
                if (Math.Abs(_stockPrice - value) < 0.001)
                    return;

                _stockPrice = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(TotalPrice));
            }
        }

        public string? ErrorMessage
        {
            set => ErrorMessageViewModel.Message = value;
        }


        public override void Dispose()
        {
            ErrorMessageViewModel.Dispose();
            StatusMessageViewModel.Dispose();
            AssetListingViewModel.Dispose();

            base.Dispose();
        }
    }
}
